--------------------
whatsdoom.com-theme
--------------------

A pelican theme for whatsdoom.com built using tailwind, daisyui

Configuration
--------------

:ABOUT_ME:
:AVATAR:

:BANNER:
:BANNER_ALL_PAGES:
:BANNER_SUBTITLE:

::CC_LICENSE:
:CUSTOM_LICENSE:

:CUSTOM_CSS:
:CUSTOM_JS:


:DISPLAY_BREADCRUMBS:
:DISPLAY_INFO_ON_INDEX:
:DISPLAY_CATEGORY_IN_BREADCRUMBS:
:DISPLAY_PAGES_ON_MENU:
:DISPLAY_CATEGORY_ON_MENU:

:HIDE_SIDEBAR:
:DISPLAY_RECENT_POSTS_ON_SIDEBAR:
:RECENT_POST_COUNT:

:SOCIAL:
:DISPLAY_TAGS_ON_SIDEBAR:

:FAVICON:
:TOUCHICON:

:SHOW_DATE_MODIFIED:
:SHOW_SERIES:
:SHOW_ARTICLE_AUTHOR:
:SHOW_ARTICLE_CATEGORY:

:PDF_PROCESSOR:
:PAGES_SORT_ATTRIBUTE:

:USE_PAGER:

Plugins
--------

Here are a list of plugins that this theme can use:

- ``tag_cloud``
- ``sitemap``
- ``series``
- ``readtime``
- ``neighbors``

Commands
---------

Watch for changes and rebuild::

    npm run dev


Production build for minified CSS::

    npm run prod
