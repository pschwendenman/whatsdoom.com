whatsdoom.com
---------------

This the repo for a personal blog served at the URL whatsdoom.com


Add theme as subtree
=====================

::

    git subtree add --prefix whatsdoom-theme git@gitlab.com:pschwendenman/whatsdoom.com-theme.git master --squash


Update the theme
=================

::

    make update_theme


Update deps
============

::

    make pip_freeze
