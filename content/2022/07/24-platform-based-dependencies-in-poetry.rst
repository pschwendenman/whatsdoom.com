------------------------------------------------------------------
Platform based dependencies in poetry
------------------------------------------------------------------

:date: 2022-07-24
:category: blog
:tags: python, poetry

Problem
--------

I have a small django project that I develop on both linux and mac. I deploy this
project using the alpine version of the python docker image.

Originally, I had installed the ``psycopg2-binary`` package on my mac for local development,
but once started to package up the app, I had issues installing the prebuilt package
on the alpine image. I switched to the ``psycopg2`` package in my ``pyproject.toml`` and didn't
really think much more about it as I was mostly developing on linux at the time. Recently, I
tried to switch back to the mac and got an error running ``poetry install``:

::

    • Installing psycopg2 (2.9.3): Failed

    EnvCommandError

    [snip]

        Error: pg_config executable not found.

        pg_config is required to build psycopg2 from source.  Please add the directory
        containing pg_config to the $PATH or specify the full executable path with the
        option:

            python setup.py build_ext --pg-config /path/to/pg_config build ...

        or with the pg_config option in 'setup.cfg'.

        If you prefer to avoid building psycopg2 from source, please install the PyPI
        'psycopg2-binary' package instead.

        For further information please check the 'doc/src/install.rst' file (also at
        <https://www.psycopg.org/docs/install.html>).


Solution
---------

After some quick bit of searching, I found the correct section of the ``poetry`` docs_, which led me to:

::

    [tool.poetry.dependencies.psycopg2]
    version = "^2.9.3"
    markers = "sys_platform == 'linux'"

    [tool.poetry.dependencies.psycopg2-binary]
    version = "^2.9.3"
    markers = "sys_platform == 'darwin'"

.. _docs: https://python-poetry.org/docs/dependency-specification/#using-environment-markers


Notes
------

To find the correct answer for ``sys_platform``, you can use `this table`_ on stackoverflow_. Or you
can check locally yourself::

    python -c "import sys; print(sys.platform)"

Example using ``docker``::

    $ docker run python:3.9.4-alpine3.13 python -c "import sys; print(sys.platform)"
    linux

.. _this table: https://stackoverflow.com/a/13874620
.. _stackoverflow: https://stackoverflow.com/questions/446209/possible-values-from-sys-platform
