About
########

I am currently a software developer in Columbus. I have been writing software
professionally since 2013. I have written software in multiple stacks and worked
as a consultant for five years until October 2020.

I worked extensively with the agile process and extreme programming. I care
passionately about team ownership and uplift. I believe that high functioning
healthy teams produce higher quality software than "rock star" individual contributors.

I am a graduate of the Ohio State University, and I studied Electrical and
Computer Engineering with a specialization in Computer Engineering. I was a
member of the Open Source Club at the Ohio State University.

Outside of software, I am interested in control theory and adaptive filtering. I
enjoy kayaking, fishing and hiking outdoors.

Thanks and enjoy,

Paul

----------

resume_

.. _resume: /resume.pdf
