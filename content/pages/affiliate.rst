================
Affiliate Links
================

:date: 2020-02-29
:status: hidden
:save_as: affiliates/index.html
:url: affiliates/


Services that I use which offer affiliate links, if you found the content
of this site helpful consider using these links before signing up:

- `Digital Ocean <https://m.do.co/c/4845066dcbcd>`_
- `Vultr <https://www.vultr.com/?ref=7125163>`_
- `Linode <https://www.linode.com/?r=129b9a3bb95e057fc4428cf56628ea5266060b5a>`_
