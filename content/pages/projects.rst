------------
Projects
------------

:save_as: projects/index.html
:url: projects/

Projects that I am currently developing or have worked on in the past.  They are
listed below with background on their purpose, progress and code location.

#. `bowlingscores.app`_

    :purpose: A webapp to track and analyze your bowling games.
    :repo: https://github.com/bowling-scores (private for now)


#. On-demand Minecraft server

    :purpose: This project provides all the configuration using ``terraform`` needed to run an "on demand" minecraft server on AWS. The
              server will automatically shutdown after five minutes of no one playing to reduce cost. The project
              also contains an AWS lambda based API and a small svelte webapp to allow users to start the EC2 instance
              when they'd like to play.
    :repo: https://github.com/paul-schwendenman/minecraft-server-host

#. `Stand-up round robin app`_

    :purpose: Tiny app for handling a random stand-up rotation with optional timer. It allows for adding and removing people as well
              as temporarily disabling a person. The app allows for skipping which will requeue a name. Additionally, the app saves the
              app state in the url for handy bookmarking and sharing.
    :repo: https://github.com/paul-schwendenman/standup-rotation

#. `Twin trimmer`_

    :purpose: Tool that can help to automate removal of duplicate files
    :repo: https://github.com/paul-schwendenman/twintrim


Legacy
-------

#. `Progress Bar (tqdm)`_ (2015)

    :purpose: Create a reusable python progress bar. It is a fork of another
        project but allows for styling of the progressbar.
        (edit: this repo was abandoned when I forked it but it now seems to have been picked up by the community)
    :repo: https://github.com/paul-schwendenman/tqdm

#. `Paste Math`_ (2013)

    :purpose: Create a pastebin type application that also includes the MathJAX javascript library for templating.
    :repo: https://github.com/paul-schwendenman/paste-math


#. `Euchre`_ (2013)

    :purpose: Develop an application to play euchre between friends. Also develop a believable computer controlled player.

#. `Tic-tac-toe`_ (2013)

    :purpose: Develop a tic-tac-toe ai that will learn with the player until it can play a perfect game.

.. _Euchre: https://github.com/paul-schwendenman/euchre
.. _Tic-tac-toe: https://github.com/paul-schwendenman/tictac
.. _Paste Math: pastemath/
.. _Progress Bar (tqdm): https://github.com/paul-schwendenman/tqdm
.. _Twin trimmer: https://github.com/paul-schwendenman/twintrim
.. _bowlingscores.app: https://bowlingscores.app
.. _Stand-up round robin app: https://paul-schwendenman.github.io/standup-rotation/
