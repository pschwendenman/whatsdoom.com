-----------
Websites
-----------

:date: 2014-01-19
:status: hidden
:save_as: links/index.html
:url: links/


"Semi-curated" list of resources, tools and fun websites:

.. contents:: Sections:

----

CLI tools
-----------

- `Screen User's Manual <http://www.gnu.org/software/screen/manual/screen.html#Getting-Startedhttp://www.gnu.org/software/screen/manual/screen.html#Getting-Started>`_

  Screen User's Manual... self explanatory

Comics
-----------

- `xkcd.com <http://xkcd.com/>`_

  A webcomic of romance, sarcasm, math, and language.

Databases
-----------

- `PostgreSQL Cheat Sheet <http://www.petefreitag.com/cheatsheets/postgresql/>`_

  self explanatory

Django
-----------

- `A Guide to Testing in Django <http://toastdriven.com/blog/2011/apr/10/guide-to-testing-in-django/>`_

  A simple guide to testing in django based on the example app from the django website.

Editors
-----------

- `Joe's Own Editor <http://joe-editor.sourceforge.net/>`_

  JOE is a full featured terminal-based screen editor.

- `Atom <https://atom.io/>`_

  A hackable text editor

Electronics
-----------

- `Electronics Tutorials <http://www.michaels-electronics-lessons.com/>`_

  Some basic electronic tutorials on Filters, Transistors, Basic Formulas, Motors...

Games
-----------

- `Maze <http://www.falstad.com/maze/>`_

  A very simple yet enjoyable maze written in Java. Its addicting.

- `Blackjack <http://www.brainjar.com/js/blackjack/blackjack.html>`_

  A fun little blackjack game.

- `Internet Arcade <https://archive.org/details/internetarcade>`_

  Library of arcade (coin-operated) video games from the 1970s through to
  the 1990s.

- `MS-DOS Games <https://archive.org/details/softwarelibrary_msdos_games>`_

  The collection includes action, strategy, adventure and other unique
  genres of game and entertainment software.

Git reference
--------------

- `Git cheatsheet <https://na1.salesforce.com/help/doc/en/salesforce_git_developer_cheatsheet.pdf>`_

  A handy git cheat sheet.

- `Git reference <http://git-scm.com/docs>`_

  One of the sites I used for doing my research about git. Really well written documentation.

Groups
-----------

- `Open Source Club <http://opensource.cse.ohio-state.edu/>`_

  The Ohio State Open Source Club is a student organization at the Ohio State University that meets weekly to discuss open source related topics.

Hobbies
-----------

- `DIY Fish keepers <http://diyfishkeepers.com/>`_

  Website devoted to DIY aquarium-related projects. Lots of neat ideas and videos.

- `Raspberry Pi powered time lapse dolly <http://www.instructables.com/id/Raspberry-Pi-powered-time-lapse-dolly-RasPiLapse/?ALLSTEPS>`_

  A project designed to make a time lapse dolly for a camera.

- `Woodworking videos, projects, and tips <http://www.woodworkingformeremortals.com/>`_

  Some wood working projects and videos

LaTeX
-----------

- `Latex Reference <http://en.wikibooks.org/wiki/LaTeX>`_

  One of the most complete LaTeX references on the internet.

- `Latex Symbols <http://www.artofproblemsolving.com/Wiki/index.php/LaTeX:Symbols>`_

  A small handy reference for symbols in latex.

- `Latex Equations <http://www.codecogs.com/latex/eqneditor.php>`_

  This site will let you make latex equations by clicking buttons. Especially helpful if you can't figure out how to get an equation to look properly. Also, it has a lot of nice examples.

Misc
-----------

- `Market Street Railway <http://www.streetcar.org/>`_

  This website brings you news and information about San Franicsco's historic streetcars and cable cars.

- `Wizard of Odds <http://wizardofodds.com/>`_

  A bunch of odds for casino games

Photography
-------------

- `Pixls.us <https://pixls.us/>`_

  Free/Open Source Photography

- `GIMP <https://www.gimp.org/>`_

  GNU Image Manipulation Program

- `Nik Collection <https://www.google.com/nikcollection/>`_

  Nik Collection is multiple image editing plug-ins and tools.

Politics
-----------

- `PolitiFact.com <http://www.politifact.com/truth-o-meter/>`_

  The truth-o-meter will teach you that everyone lies.

- `The World's Smallest Political Quiz <http://www.theadvocates.org/quiz>`_

  Yup

Programming
-------------

- `Eric Phelps.com <http://www.ericphelps.com/>`_

  Tutorials and tricks having to do with windows scripting, from batch to VB Scripts, as well as some nifty free programs.

- `Lisp Comparison Chart <http://hyperpolyglot.org/lisp>`_

  A side-by-side reference sheet for Common Lisp, Scheme, Clojure, Emacs Lisp.

- `Rosetta Code <http://rosettacode.org/>`_

  Has solutions to the same task in as many languages as possible.

Sports
-----------

- `Football Rules <http://football.calsci.com/>`_

  Football formations, defensive and offensive positions, etc.

- `506 Sports <http://506sports.com/>`_

  This website allows you to view the national television network of every NFL, CFB, and MLB game throughout their seasons and playoffs.

System Admin
-------------

- `Fail2Ban <http://www.the-art-of-web.com/system/fail2ban-log/>`_

  fail2ban log file and ways to analyze it using simple command-line tools such as awk and grep.

Web
-----------

- `CSS3 Cheatsheet <https://websitesetup.org/css3-cheat-sheet/>`_

  Complete CSS cheat sheet. Updated with new CSS3 tags.

- `W3Schools <http://www.w3schools.com/>`_

  Tutorials in just about part of client-side web development. HTML, CSS, Javascript...

- `CSS Cheatsheet <http://coding.smashingmagazine.com/2009/07/13/css-3-cheat-sheet-pdf/>`_

  A really helpful and complete CSS cheat sheet.
