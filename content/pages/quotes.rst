----------
Quotes
----------

:date: 2014-01-19
:save_as: quotes/index.html
:url: quotes/

Quotes I have enjoyed over the years:

--------

.. epigraph::
  Against stupidity, even the gods themselves struggle in vain.

  -- Isaac Asimov

.. epigraph::
  The most incomprehensible thing about the world is that it is at all
  comprehensible.

  -- Albert Einstein

.. epigraph::
  Whenever you have an efficient government you have a dictatorship.

  -- Harry S. Truman

.. epigraph::
  Egotism is the anesthetic that dulls the pain of stupidity.

  -- Frank Leahy

.. epigraph::
  I never vote for anyone; I always vote against.

  -- W. C. Fields

.. epigraph::
  Frisbeetarianism is the belief that when you die, your soul goes up on the
  roof and gets stuck.

  -- George Carlin

.. epigraph::
  Any man who can drive safely while kissing a pretty girl is simply not
  giving the kiss the attention it deserves.

  -- Albert Einstein

.. epigraph::
  The problem with political jokes is they get elected.

  -- Henry Cate VII

.. epigraph::
  If I only had a little humility, I'd be perfect.

  -- Ted Turner

.. epigraph::
  Aways acknowledge a fault.  This will throw those in authority off their
  guard and give you an opportunity to commit more.

  -- Mark Twain

.. epigraph::
  I have discovered that all human evil comes from this, man's being unable
  to sit still in a room.

  -- Blaise Pascal

.. epigraph::
  A lot of people mistake a short memory for a clear conscience.

  -- Doug Larson

.. epigraph::
  The best argument against democracy is a five-minute conversation with the
  average voter.

  -- Winston Churchill

.. epigraph::
  Once you've learned to study in a bathing suit on the grass with muscled
  men throwing frisbees over your head, you can accomplish almost anything.

  -- Susan Rice

.. epigraph::
  Whenever people agree with me I always feel I must be wrong.

  -- Oscar Wilde

.. epigraph::
  We hang the petty thieves and appoint the great ones to public office.

  -- Aesop

.. epigraph::
  A diplomat...  is a person who can tell you to go to hell in such a way
  that you actually look forward to the trip.

  -- Caskie Stinnett

.. epigraph::
  Never keep up with the Joneses. Drag them down to your level.

  -- Quentin Crisp

.. epigraph::
  Once the game is over, the King and the pawn go back in the same box.

  -- Italian Proverb

.. epigraph::
  Education is a method whereby one acquires a higher grade of prejudices.

  -- Laurence J. Peter

.. epigraph::
  Personally I'm always ready to learn, although I do not always like being taught.

  -- Winston Churchill

.. epigraph::
  I became a feminist as an alternative to becoming a masochist.

  -- Sally Kempton

.. epigraph::
  There is no fate that cannot be surmounted by scorn.

  -- Albert Camus

.. epigraph::
  The scientific name for an animal that doesn't either run from or fight
  its enemies is lunch.

  -- Michael Friedman

.. epigraph::
  The key to being a good manager is keeping the people who hate me away
  from those who are still undecided.

  -- Casey Stengel

.. epigraph::
  People are, if anything, more touchy about being thought silly than they
  are about being thought unjust.

  -- E. B. White

.. epigraph::
  I have left orders to be awakened at any time in case of national
  emergency, even if I'm in a cabinet meeting.

  -- Ronald Reagan

.. epigraph::
  The truth that makes men free is for the most part the truth which men
  prefer not to hear.

  -- Herbert Agar

.. epigraph::
  Some men are born mediocre, some men achieve mediocrity, and some men have
  mediocrity thrust upon them.

  -- Joseph Heller

.. epigraph::
  We know what happens to people who stay in the middle of the road.  They
  get run over.

  -- Aneurin Bevan

.. epigraph::
  That's the secret to life...  replace one worry with another....

  -- Charles M.  Schulz

.. epigraph::
  Thus the metric system did not really catch on in the States, unless you
  count the increasing popularity of the nine-millimeter bullet.

  -- Dave Barry

.. epigraph::
  Any American who is prepared to run for president should automatically, by
  definition, be disqualified from ever doing so.

  -- Gore Vidal

.. epigraph::
  There is something that is much more scarce, something rarer than ability.
  It is the ability to recognize ability.

  -- Robert Half

.. epigraph::
  An atheist is a man who has no invisible means of support.

  -- John Buchan

.. epigraph::
  My theory is that if you look confident you can pull off anything - even
  if you have no clue what you're doing.

  -- Jessica Alba

.. epigraph::
  Love thy neighbour as yourself, but choose your neighbourhood.

  -- Louise Beal

.. epigraph::
  Eagles may soar, but weasels don't get sucked into jet engines.

  -- John Benfield

.. epigraph::
  Men never do evil so completely and cheerfully as when they do it from a
  religious conviction.

  -- Blaise Pascal

.. epigraph::
  If you torture the data long enough, it will confess.

  -- Ronald Coase

.. epigraph::
  I believe that every human has a finite number of heart-beats.  I don't
  intend to waste any of mine running around doing exercises.

  -- Buzz Aldrin

.. epigraph::
  I believe that professional wrestling is clean and everything else in the
  world is fixed.

  -- Frank Deford

.. epigraph::
  I wish people who have trouble communicating would just shut up.

  -- Tom Lehrer

.. epigraph::
  Rudeness is the weak man's imitation of strength.

  -- Eric Hoffer

.. epigraph::
  I know that you believe that you understood what you think I said, but I
  am not sure you realize that what you heard is not what I meant.

  -- Robert McCloskey

.. epigraph::
  Si vis pacem para bellum.

  -- Vegetius

.. epigraph::
  Stoop and you'll be stepped on; stand tall and you'll be shot at.

  -- Carlos A.  Urbizo

.. epigraph::
  Law and order exist for the purpose of establishing justice and when they
  fail in this purpose they become the dangerously structured dams that block
  the flow of social progress.

  -- Martin Luther King, Jr.

.. epigraph::
  I'm a great believer in luck, and I find the harder I work the more I have
  of it.

  -- Thomas Jefferson

.. epigraph::
  Religion is to spirituality what pornography is to sexuality.

  -- Bruce M.  Foster

.. epigraph::
  An atheist is a man who watches a Notre Dame - Southern Methodist
  University game and doesn't care who wins.

  -- Dwight D.  Eisenhower

.. epigraph::
  How far you can go without destroying from within what you are trying to
  defend from without?

  -- Dwight D.  Eisenhower

.. epigraph::
  I hate war as only a soldier who has lived it can, only as one who has
  seen its brutality, its futility, its stupidity.

  -- Dwight D.  Eisenhower

.. epigraph::
  I have found out in later years that we were very poor, but the glory of
  America is that we didn't know it then.

  -- Dwight D.  Eisenhower

.. epigraph::
  If men can develop weapons that are so terrifying as to make the thought
  of global war include almost a sentence for suicide, you would think that
  man's intelligence and his comprehension...  would include also his ability
  to find a peaceful solution.

  -- Dwight D.  Eisenhower

.. epigraph::
  We are going to have peace even if we have to fight for it.

  -- Dwight D.  Eisenhower

.. epigraph::
  Anger is a Condition in Which the Tongue Works Faster than the Mind.

  -- Unknown

.. epigraph::
  The definition of insanity is doing the same thing over and over again and
  expecting different results.

  -- Albert Einstein

.. epigraph::
  A person who never made a mistake never tried anything new.

  -- Albert Einstein

.. epigraph::
  As far as the laws of mathematics refer to reality, they are not certain,
  and as far as they are certain, they do not refer to reality.

  -- Albert Einstein

.. epigraph::
  A diplomat is a man who always remembers a woman's birthday but never
  remembers her age.

  -- Robert Frost

.. epigraph::
  The reason why worry kills more people than work is that more people worry
  than work.

  -- Robert Frost

.. epigraph::
  The world is full of willing people, some willing to work, the rest
  willing to let them.

  -- Robert Frost

.. epigraph::
  Love is the irresistible desire to be irresistibly desired.

  -- Robert Frost

.. epigraph::
  Depression occurs when one looks back with no pride, and looks forward
  with no hope.

  -- Robert Frost

.. epigraph::
  Jealousy feeds upon suspicion, and it turns into fury or it ends as soon
  as we pass from suspicion to certainty.

  -- François de La Rochefoucauld

.. epigraph::
  Government does not solve problems; it subsidizes them.

  -- Ronald Reagan

.. epigraph::
  I am not worried about the deficit.  It is big enough to take care of
  itself.

  -- Ronald Reagan

.. epigraph::
  A soldier will fight long and hard for a bit of colored ribbon.

  -- Napoleon Bonaparte

.. epigraph::
  There is a tragic flaw in our precious Constitution, and I don't know what
  can be done to fix it.  This is it: Only nut cases want to be president.

  -- Kurt Vonnegut

.. epigraph::
  I know not with what weapons World War III will be fought, but World War
  IV will be fought with sticks and stones.

  -- Albert Einstein

.. epigraph::
  Democracy means that anyone can grow up to be president, and anyone who
  doesn't grow up can be vice president.

  -- Johnny Carson

.. epigraph::
  The trouble with fighting for human freedom is that one spends most of
  one's time defending scoundrels.  For it is against scoundrels that
  oppressive laws are first aimed, and oppression must be stopped at the
  beginning if it is to be stopped at all.

  -- H.  L.  Mencken

.. epigraph::
  Courage is the art of being the only one who knows you're scared to death.

  -- Harold Wilson

.. epigraph::
  If all the girls who attended the Yale prom were laid end to end, I
  wouldn't be a bit surprised.

  -- Dorothy Parker

.. epigraph::
  As long as people will accept crap, it will be financially profitable to
  dispense it.

  -- Dick Cavett

.. epigraph::
  Good judgement comes from experience.  Experience comes from bad
  judgement.

  -- Unknown

.. epigraph::
  Progress might have been all right once, but it has gone on too long.

  -- Ogden Nash

.. epigraph::
  You get fifteen democrats in a room, and you get twenty opinions.

  -- Senator Patrick Leahy

.. epigraph::
  If you are not criticized, you may not be doing much.

  -- Donald H.  Rumsfeld

.. epigraph::
  Human history becomes more and more a race between education and
  catastrophe.

  -- H.  G.  Wells

.. epigraph::
  Golf and sex are about the only things you can enjoy without being good at.

  -- Jimmy Demaret

.. epigraph::
  From the moment I picked up your book until I laid it down, I was
  convulsed with laughter.  Some day I intend reading it.

  -- Groucho Marx

.. epigraph::
  The secret of staying young is to live honestly, eat slowly, and lie about
  your age.

  -- Lucille Ball

.. epigraph::
  The true measure of a man is how he treats someone who can do him
  absolutely no good.

  -- Samuel Johnson

.. epigraph::
  Advertising may be described as the science of arresting the human
  intelligence long enough to get money from it.

  -- Stephen Leacock

.. epigraph::
  What is the difference between unethical and ethical advertising?
  Unethical advertising uses falsehoods to deceive the public; ethical
  advertising uses truth to deceive the public.

  -- Vilhjalmur Stefansson

.. epigraph::
  A man's respect for law and order exists in precise relationship to the
  size of his paycheck.

  -- Adam Clayton Powell Jr.

.. epigraph::
  Little by little we human beings are confronted with situations that give
  us more and more clues that we are not perfect.

  -- Fred Rogers

.. epigraph::
  If the freedom of speech is taken away then dumb and silent we may be led,
  like sheep to the slaughter.

  -- George Washington

.. epigraph::
  Am I not destroying my enemies when I make friends of them?

  -- Abraham Lincoln

.. epigraph::
  Progress isn't made by early risers.  It's made by lazy men trying to find
  easier ways to do something.

  -- Robert A.  Heinlein

.. epigraph::
  Anti-intellectualism has been a constant thread winding its way through
  our political and cultural life, nurtured by the false notion that
  democracy means that 'my ignorance is just as good as your knowledge.'

  -- Isaac Asimov

.. epigraph::
  The fundamental cause of the trouble is that in the modern world the stupid are cocksure while the intelligent are full of doubt

  -- Bertrand Russell - the Triumph of Stupidity (1933)

.. epigraph::
  The best lack all conviction, while the worst are full of passionate intensity.

  -- Yeats, 1919

.. epigraph::
  Build a man a fire, and he'll be warm for a day. Set a man on fire, and
  he'll be warm for the rest of his life.

  -- Terry Pratchett
