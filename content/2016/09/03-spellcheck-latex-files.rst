---------------------------------------
Spell checking latex files with aspell
---------------------------------------

:date: 2016-09-03
:category: blog
:tags: latex, cli tools, aspell

Aspell has a mode which allows a user check the spelling
of the document without having to skip through all the latex commands.

::

  aspell --lang=en --mode=tex check file.tex
