--------------------------------------------
Finding files without New Lines at the End
--------------------------------------------

:date: 2016-09-05
:category: blog
:tags: cli tools, git

Motivation
===========

Not having newlines at the ends of files messes up lots of command line
operations such as ``wc``, ``>>``, ``git`` and ``cat``.

POSIX definition of a line:

  3.206 Line
  A sequence of zero or more non- <newline> characters plus a terminating <newline> character.

:source: http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_206
:more: http://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline

Examples
=========

Example 1:

Given two files ``1.txt`` and ``2.txt`` without newlines at the end::

    $ cat 1.txt 2.txt
    onetwo

The same two files with newlines::

    $ cat 1.txt 2.txt
    one
    two

Example 2:

Appending to the end of a file doesn't work properly without a newline::

    $ cat .gitignore
    *.pyc
    $ echo "*~" >> .gitignore
    $ cat .gitignore
    *.pyc*~

Example 3:

Word count (``wc``) doesn't correctly count lines::

    $ cat missing.txt | wc -l
    0

Commands
=========

Finding missing newlines
-------------------------

Recursively find files missing trailing newlines in directory::

    find . -type f -print0 | xargs -0 -L1 bash -c 'test "$(tail -c 1 "$0")" && echo "No new line at end of $0"'

Find files missing trailing newlines in a git repo::

    git ls-files -z | xargs -0 -L1 bash -c 'test "$(tail -c 1 "$0")" && echo "No new line at end of $0"'

Adding missing newlines
------------------------

- Using your editor of choice, go to the last line and add newline

- Ed::

    ed -s file <<< w

- Sed::

    sed -i -e '$a\' file
