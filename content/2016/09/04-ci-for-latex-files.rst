--------------------
CI for latex files
--------------------

:date: 2016-09-04
:category: blog
:tags: continuous integration, travis, latex

I wanted to make a build pipeline for my resume which is written
in Latex. Using Travis CI, I was able to add a build that
would upload an automatically compiled PDF
as a release for every tag in a git repository.

.. code-block:: yaml

    sudo: required
    dist: trusty
    before_install:
    - sudo apt-get -qq update && sudo apt-get install -y --no-install-recommends latexmk
      texlive-fonts-extra texlive-fonts-recommended texlive-latex-base texlive-latex-extra
      texlive-latex-recommended
    script:
    - latexmk -pdf resume.tex
    deploy:
      provider: releases
      api_key:
        secure: <key from travis CLI script>
      file:
      - resume.pdf
      skip_cleanup: true
      on:
        tags: true

Breakdown of YML
=================

before_install
---------------

packages needed to build your latex document mileage will vary:

- latexmk
- texlive-fonts-extra
- texlive-fonts-recommended
- texlive-latex-base
- texlive-latex-extra
- texlive-latex-recommended

latexmk
--------

Automatic LaTeX document generation tool

api_key
--------

created using the Travis CLI gem

install::

  gem install travis -v 1.8.2 --no-rdoc --no-ri


generate::

  travis setup releases



:travis help: https://harshjv.github.io/blog/setup-latex-pdf-build-using-travis-ci/
:latexmk: http://stackoverflow.com/questions/1465726/latex-how-do-i-know-to-stop-building-programatically
:resume repo: https://github.com/paul-schwendenman/resume
