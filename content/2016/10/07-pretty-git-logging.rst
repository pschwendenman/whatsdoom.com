======================================
Pretty printing of git commit history
======================================

:date: 2016-10-07
:category: blog
:tags: git

Helpful git logging aliases
============================

Helpful aliases for viewing git commit history: logg, logga, loggs, loggsa,
logd, logda, logdr, and logdra.

The aliases combine to utilize the same set of flags in different combinations:

:--color: show colored
:--graph: text-based graphical representation of the commit history
:--date-order: show no parents before all of its children are shown
:--decorate: print out the ref names (branches/tags) of any commits that are shown.
:--oneline: single line per commit with shortened  commit SHA
:--simplify-by-decoration: show only commits that are referred to by a branch or tag
:--all: show all commits
:--date=relative (%ar): shows dates relative to the current time, e.g. "2 hours ago"

Git Config Aliases
===================

::

    [alias]
        #quick look at all repo
        loggsa = log --color --date-order --graph --oneline --decorate --simplify-by-decoration --all
        #quick look at active branch (or refs pointed)
        loggs  = log --color --date-order --graph --oneline --decorate --simplify-by-decoration
        #extend look at all repo
        logga  = log --color --date-order --graph --oneline --decorate --all
        #extend look at active branch
        logg   = log --color --date-order --graph --oneline --decorate
        #Look with date
        logda  = log --color --date-order --date=local --graph --format=\"%C(auto)%h%Creset %C(blue bold)%ad%Creset %C(auto)%d%Creset %s\" --all
        logd   = log --color --date-order --date=local --graph --format=\"%C(auto)%h%Creset %C(blue bold)%ad%Creset %C(auto)%d%Creset %s\"
        #Look with relative date
        logdra = log --color --date-order --graph --format=\"%C(auto)%h%Creset %C(blue bold)%ar%Creset %C(auto)%d%Creset %s\" --all
        logdr = log --color --date-order --graph --format=\"%C(auto)%h%Creset %C(blue bold)%ar%Creset %C(auto)%d%Creset %s\"


Examples
=========

Simple single line per commit printing of current branch::

  $ git logg -n 5
  * 8062af6 (HEAD -> master) POST: Pretty printing json and xml
  * 9a600fd (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  * feaa32a (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * 6b2746e Update repo to use new submodules - Change pelican conf to point to new theme - Migrate gitlab-ci to use submodules - Rename reqs.txt to requirements.txt
  * d1408c6 Add plugins and themes as submodules


Single line printing of all commits::

  $ git logga -n 5
  * 8062af6 (HEAD -> master) POST: Pretty printing json and xml
  | * 1662f36 (origin/new-post) POST: Pretty printing json and xml
  |/
  * 9a600fd (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  | * e4300c7 (heroku/master, heroku) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * | feaa32a (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme

Single line printing of commits with decoration (branches,
tags, etc.)::

  $  git loggs -n 5
  * 8062af6 (HEAD -> master) POST: Pretty printing json and xml
  * 9a600fd (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  * feaa32a (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * f7fc76b (origin/old-theme, old-theme) Tags are multicolumn
  * 4846adf Initial Commit

Single line printing of commits with decoration (branches, tags, etc.) from
all commits::

  $  git loggsa -n 5
  * 8062af6 (HEAD -> master) POST: Pretty printing json and xml
  | * 1662f36 (origin/new-post) POST: Pretty printing json and xml
  |/
  * 9a600fd (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  | * e4300c7 (heroku/master, heroku) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * | feaa32a (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  |/

Single line printing of commits on current branch with date
included::

  $  git logd -n 5
  * 8062af6 Thu Oct 6 23:09:23 2016  (HEAD -> master) POST: Pretty printing json and xml
  * 9a600fd Thu Oct 6 22:54:59 2016  (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  * feaa32a Tue Sep 13 01:09:37 2016  (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * 6b2746e Sun Oct 2 20:10:38 2016  Update repo to use new submodules - Change pelican conf to point to new theme - Migrate gitlab-ci to use submodules - Rename reqs.txt to requirements.txt
  * d1408c6 Sun Oct 2 18:48:51 2016  Add plugins and themes as submodules

Single line printing of all commits with date included::

  $ git logda -n 5
  * 8062af6 Thu Oct 6 23:09:23 2016  (HEAD -> master) POST: Pretty printing json and xml
  | * 1662f36 Thu Oct 6 23:09:23 2016  (origin/new-post) POST: Pretty printing json and xml
  |/
  * 9a600fd Thu Oct 6 22:54:59 2016  (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  | * e4300c7 Tue Sep 13 01:09:37 2016  (heroku/master, heroku) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * | feaa32a Tue Sep 13 01:09:37 2016  (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme


Single line printing of commits on current branch with relative
date included::

  $ git logdr -n 5
  * 8062af6 45 minutes ago  (HEAD -> master) POST: Pretty printing json and xml
  * 9a600fd 59 minutes ago  (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  * feaa32a 3 weeks ago  (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * 6b2746e 4 days ago  Update repo to use new submodules - Change pelican conf to point to new theme - Migrate gitlab-ci to use submodules - Rename reqs.txt to requirements.txt
  * d1408c6 4 days ago  Add plugins and themes as submodules

Single line printing of all commits with relative date included::

  $ git logdra -n 5
  * 8062af6 44 minutes ago  (HEAD -> master) POST: Pretty printing json and xml
  | * 1662f36 44 minutes ago  (origin/new-post) POST: Pretty printing json and xml
  |/
  * 9a600fd 58 minutes ago  (origin/update-post, origin/master, origin/HEAD) Capitalize blog post title
  | * e4300c7 3 weeks ago  (heroku/master, heroku) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme
  * | feaa32a 3 weeks ago  (search) Install tipue search - Adding basics for search - Install beautifulsoup (required for plugin) - Update to latest theme

Commands for setting aliases
=============================

To quickly set a few of the aliases on a unfamiliar computer::

    git config --global alias.logg 'log --color --date-order --graph --oneline --decorate'
    git config --global alias.logga 'log --color --date-order --graph --oneline --decorate --all'

    git config --global alias.loggs 'log --color --date-order --graph --oneline --decorate --simplify-by-decoration'
    git config --global alias.loggsa 'log --color --date-order --graph --oneline --decorate --simplify-by-decoration --all'


External sites
===============

:my gitconfig: https://github.com/paul-schwendenman/misc/blob/master/dotfiles/gitconfig
:stackoverflow discussion: http://stackoverflow.com/questions/1057564/pretty-git-branch-graphs/22875140#22875140
:another similar gitconfig: https://github.com/albfan/dotfiles/blob/master/gitconfig
