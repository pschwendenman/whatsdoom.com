----------------------------------------
Pretty Printing from the Commmand Line
----------------------------------------

:date: 2016-10-06
:category: blog
:tags: cli tools, json, xml

JSON
-----

The python standard library provides a json library that can be used to pretty
print json.

::

    $  echo '{"this": ["that"]}' | python -m json.tool
    {
      "this": [
          "that"
      ]
    }

XML
----

::

    $  echo '<root><foo a="b">lorem</foo><bar value="ipsum" /></root>' | xmllint --format -
    <?xml version="1.0"?>
    <root>
      <foo a="b">lorem</foo>
      <bar value="ipsum"/>
    </root>


Install on ubuntu::

    sudo apt-get install libxml2-utils

recommendation from: http://stackoverflow.com/questions/16090869/how-to-pretty-print-xml-from-the-command-line
