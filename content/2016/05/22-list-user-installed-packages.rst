-------------------------------
List User Installed Packages
-------------------------------

:date: 2016-05-22
:category: blog
:tags: ubuntu, cli tools, debian

I started thinking about doing a fresh linux install and was worried
that I might not remember all the packages that I had manually installed.
A quick search revealed a one-liner that compares the currently
installed packages to the list of packages that were available
initially to produce a nice list of user installed packages.

::

    comm -23 \
      <(apt-mark showmanual | sort -u) \
      <(gzip -dc /var/log/installer/initial-status.gz | \
        sed -n 's/^Package: //p' | sort -u)

:source: http://askubuntu.com/questions/2389/generating-list-of-manually-installed-packages-and-querying-individual-packages
