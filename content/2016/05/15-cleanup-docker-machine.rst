--------------------------
Clean up Docker Instances
--------------------------

:date: 2016-05-15
:category: blog
:tags: docker, docker-machine

Three useful commands to clean up docker if you
run out of space


Clean up Containers
--------------------

::

    docker rm $(docker ps -a -q)

Clean up Images
----------------

::

    docker rmi $(docker images | grep "^<none>" | awk '{print $3}')

Clean up Volumes
-----------------

::

    docker volume rm $(docker volume ls -qf dangling=true)
