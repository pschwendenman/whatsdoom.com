----------------------------
Spinning down a Harddrive
----------------------------

:date: 2016-07-01
:category: blog
:tags: cli tools, ubuntu, hard drive

I wanted to spin down a hard drive before disconnecting. I have an external hard
drive and even after I unmounted the drive the disks were still
spinning.

Find the device label of the mounted drive::

    $ lsblk
    NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
    sda    253:0    0  20G  0 disk
    └─sda1 253:1    0  20G  0 part /
    sdb    253:0    0  10G  0 disk
    └─sdb1 253:1    0  10G  0 part /media/drive/


Running ``mount`` with no arguments will also help to find the device label

To umount the partitions on the drive::

    udisks --unmount /dev/sdb1

:note: It is extremely important to ensure that you are unmounting the
       correct device.

Finally, use ``udisks`` to stop the drive::

    udisks --detach /dev/sdb

``udisk`` can be installed on Debian systems using ``apt-get``::

    sudo apt-get install udisks


:source: http://askubuntu.com/questions/56270/how-can-i-spin-down-external-hard-drive
