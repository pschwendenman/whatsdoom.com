------------------------------
Signing a multi page PDF
------------------------------

:date: 2015-08-13
:category: blog
:tags: gimp, pdf, eSigning, image magick

Background
-----------

Rather than printing a document, filling it out by hand and then scanning to
so that you are able to return it electronically, use Gimp and ImageMagick to
sign documents electronically.

Instructions
-------------

1. First, make sure to have a png image with the signature on a transparent
   background.

#. Next, open the pdf file in gimp. Each page of the pdf file should now be
   its own layer in gimp.

#. Insert the signature as a layer in gimp. Move the layer to the correct order
   and move it above the signature line using the scale and move tools.

#. Merge the signature layer and the layer for the pdf page.

#. If a date is needed the text tool can be used to add text on the document
   again dragging the text to the proper location before merging the text layer
   with the correct page.

#. Check to ensure that the document still has the same number of layers as it
   (same as pages of original document) had on import.

#. All the fields should now be correctly filled.

#. Reverse the order of the layers. A shortcut can be found in the menu::

    Layer -> Stack -> Reverse Layer Order

#. Export the file as an mng file. i.e. ``file.mng``

#. Use ``convert`` to transform the mng file into a pdf file. Like this::

    convert file.mng file.pdf

#. The pdf file should now be ready.

Additional information
-----------------------

Making a signature file
=========================

1. Obtain an image of your signature by:
    a. On a plain white sheet of paper signing your name and then taking
       a picture or scanning the document.
    #. Use a stylus to make draw a signature on your phone screen.
#. Open the file in gimp.
#. Add an alpha channel by going to::

    Layer -> Transparency -> Add Alpha Channel

#. Used the fuzzy select tool to remove the background by clicking on the
   white background sections and then pressing the ``Del`` key or from the menu
   ``Edit -> Clear``

#. Make sure to delete any white sections in loops caused by cursive L's for
   example.

#. Save the image as a png file.

Requirements
=============

- Gimp
- ImageMagick
