-----------------------------------
Pushing to pypi
-----------------------------------

:date: 2015-07-12
:category: blog
:tags: python, pypi


Instructions
-------------

1. Make setup.py in your project

    .. code:: python

        from setuptools import setup

        setup(name='project',
              version='1.0',
              description='thing do-er',
              author='Your Name',
              url='http://path.to.website',
              py_modules=[''],
              classifiers=['Environment :: Console',
                           'Intended Audience :: Developers',
                           'License :: OSI Approved :: MIT License',
                           'Programming Language :: Python :: 3.4',
                           'Operating System :: POSIX :: Linux',
                           'Topic :: Utilities', ],
              entry_points={'console_scripts': ['commandline = module:function'], }, )

    There is lots more that you can do in a setup.py file. This example  aims to
    get you started with the bare minimum required.


#. Make a pypi account

    If you do not already have a pypi account now is the time to make one.

    Try going to this `form`_

    .. _form: https://pypi.python.org/pypi?%3Aaction=register_form

#. Make ~/.pypirc

    ::

        [distutils]
        index-servers=pypi

        [pypi]
        repository = https://pypi.python.org/pypi
        username = [your pypi account username]
        password = [your password]


#. Make the egg file::

    python setup.py sdist

#. Register the project::

    python setup.py register

#. Upload the egg::

    python setup.py sdist upload


Additional Information
-----------------------

Classifiers List
=================


    For a full list of the pypi classifiers go here:
    https://pypi.python.org/pypi?%3Aaction=list_classifiers

New releases
=============

    Once you are ready to release a new version of your project, be sure to
    change the version and upload it with step 6
