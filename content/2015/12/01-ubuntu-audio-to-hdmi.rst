---------------------------------------------
Configure pulseaudio output via command line
---------------------------------------------

:date: 2015-12-01
:category: blog
:tags: ubuntu, pulseaudio, hdmi

Changing the currently active audio output

List available audio devices
-----------------------------

First, list all the available output devices like so::

    pacmd list-cards

Or similarly::

    pactl list cards


Card Index
===========

Index of the card is found in the first couple lines::

    $ pacmd list-cards
    Welcome to PulseAudio! Use "help" for usage information.
    >>> 1 card(s) available.
        index: 0
    	name: <alsa_card.pci-0000_00_1b.0>


Profile name
=============

And the profile name options can be found further below in the output::

    ---- clip ----
    profiles:
      	input:analog-stereo: Analog Stereo Input (priority 60, available: unknown)
    		output:analog-stereo: Analog Stereo Output (priority 6000, available: unknown)
    		output:analog-stereo+input:analog-stereo: Analog Stereo Duplex (priority 6060, available: unknown)
    		output:hdmi-stereo: Digital Stereo (HDMI) Output (priority 5400, available: unknown)
    		output:hdmi-stereo+input:analog-stereo: Digital Stereo (HDMI) Output + Analog Stereo Input (priority 5460, available: unknown)
    		output:hdmi-surround: Digital Surround 5.1 (HDMI) Output (priority 300, available: unknown)
    		output:hdmi-surround+input:analog-stereo: Digital Surround 5.1 (HDMI) Output + Analog Stereo Input (priority 360, available: unknown)
    		off: Off (priority 0, available: unknown)
    ---- clip ----


Print active audio device
==========================

Another section to look for in the output is the currently active device.

Or print it directly::

  $ pacmd list-cards | grep "active profile"
	   active profile: <output:analog-stereo+input:analog-stereo>

Change audio device
---------------------

Next, use ``pactl`` to change the current audio device.

::

    pacmd set-card-profile <cardindex> <profilename>

Examples
=========

    pactl set-card-profile 0 output:hdmi-audio

For stereo::

    pactl set-card-profile 0 output:hdmi-stereo

Surround sound::

    pactl set-card-profile 0 output:hdmi-surround


Source_

.. _Source: http://askubuntu.com/questions/63599/configuring-hdmi-audio-via-command-line


Changing the volume
---------------------

It seems that ``alsamixer`` may still be the best option for changing the volume
on the command line.

::

    alsamixer
