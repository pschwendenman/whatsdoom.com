-----------------------------------
Screenshot with mplayer
-----------------------------------

:date: 2015-06-04 
:category: blog
:tags: mplayer, video, screenshot, photography

To take screenshots with ``mplayer``, you need to first run mplayer with a
special flag.

::

	mplayer -vf screenshot movie001.mp4

Then once the movie is playing you can screen shot any frame by hitting the
's' key.  ``mplayer`` will name the files starting with shot0001.png.

When I first tried this command I got a strange error, saying that it could
not make the screenshot.  Look into the log lines and you may seem something
like this::

    sending VFCTRL_SCREENSHOT!
    *** screenshot 'shot0001.png' ***

    PNG Error opening shot0001.png for writing!
    A:  27.7 V:  27.7 A-V:  0.075 ct: -0.037   0/  0 83%  3%  1.3% 0 0 
    sending VFCTRL_SCREENSHOT!
    *** screenshot 'shot0002.png' ***

    PNG Error opening shot0002.png for writing!

At this point I found two solutions. Trying moving the current directory
(which is the directory that ``mplayer`` saves the files) to a different
path.  Or try using a different version of mplayer.  I would suggest a fork
of ``mplayer`` called ``mpv``.  In ``mpv`` screenshots can be taken without
any additional flags.  Again use 's' while the movie is playing to take the
actual screenshot.

::

	mpv movie001.mp4

Additional tips
----------------

``mplayer`` will step frame-by-frame while the video is paused by hitting
'.' This should help to land that perfect shot.  (In ``mpv`` you can step
back with ',')

``mplayer`` offers a host of nice functionality to move through files. Try
looking in the man pages ofr both programs for more information.

::

	man mplayer
	man mpv

