-----------------------------------
Manually Connecting Ethernet
-----------------------------------

:date: 2014-09-05
:category: blog
:tags: ethernet, cli tools

I broke my computers internet installing wicd.

Getting internet back
----------------------

1. Check the interfaces

	::

		$ ifconfig

#. Check eth0 explicitly

	For whatever reason, my eth0 wasn't showing up until I explicitly
	specified it.

	::

		$ ifconfig eth0

#. Get an ip address

	::

		$ sudo ifconfig eth0 192.168.1.100

#. Connect to the outside internet

	::

		sudo route add -net 0.0.0.0 gw 192.168.1.1

#. Make dns resolve

	- Edit /etc/resolve.conf to include::

		nameserver 8.8.8.8
		nameserver 8.8.4.4

Fixing wicd
------------

1. Try wicd-curses

	Wicd-curses fails to start up. With a long python traceback

	.. code:: pytb

            Can't connect to the daemon, trying to start it automatically...
            Traceback (most recent call last):
              File "/usr/share/wicd/curses/wicd-curses.py", line 1043, in <module>
                setup_dbus()
              File "/usr/share/wicd/curses/wicd-curses.py", line 1031, in setup_dbus
                dbus_ifaces = dbusmanager.get_dbus_ifaces()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 36, in
            get_dbus_ifaces
                return DBUS_MANAGER.get_dbus_ifaces()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 62, in
            get_dbus_ifaces
                if not self._dbus_ifaces: connect_to_dbus()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 48, in
            connect_to_dbus
                return DBUS_MANAGER.connect_to_dbus()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 79, in
            connect_to_dbus
                proxy_obj = self._bus.get_object("org.wicd.daemon", '/org/wicd/daemon')
              File "/usr/lib/python2.7/dist-packages/dbus/bus.py", line 241, in
            get_object
                follow_name_owner_changes=follow_name_owner_changes)
              File "/usr/lib/python2.7/dist-packages/dbus/proxies.py", line 248, in
            __init__
                self._named_service = conn.activate_name_owner(bus_name)
              File "/usr/lib/python2.7/dist-packages/dbus/bus.py", line 180, in
            activate_name_owner
                self.start_service_by_name(bus_name)
              File "/usr/lib/python2.7/dist-packages/dbus/bus.py", line 278, in
            start_service_by_name
                'su', (bus_name, flags)))
              File "/usr/lib/python2.7/dist-packages/dbus/connection.py", line 651, in
            call_blocking
                message, timeout)
            dbus.exceptions.DBusException: org.freedesktop.DBus.Error.ServiceUnknown:
            The name org.wicd.daemon was not provided by any .service files
            Error in sys.excepthook:
            Traceback (most recent call last):
              File "/usr/lib/python2.7/dist-packages/apport_python_hook.py", line 96, in
            apport_excepthook
                dbus_service_unknown_analysis(exc_obj, pr)
              File "/usr/lib/python2.7/dist-packages/apport_python_hook.py", line 172,
            in dbus_service_unknown_analysis
                cp = ConfigParser(interpolation=None)
            TypeError: __init__() got an unexpected keyword argument 'interpolation'

            Original exception was:
            Traceback (most recent call last):
              File "/usr/share/wicd/curses/wicd-curses.py", line 1043, in <module>
                setup_dbus()
              File "/usr/share/wicd/curses/wicd-curses.py", line 1031, in setup_dbus
                dbus_ifaces = dbusmanager.get_dbus_ifaces()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 36, in
            get_dbus_ifaces
                return DBUS_MANAGER.get_dbus_ifaces()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 62, in
            get_dbus_ifaces
                if not self._dbus_ifaces: connect_to_dbus()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 48, in
            connect_to_dbus
                return DBUS_MANAGER.connect_to_dbus()
              File "/usr/lib/python2.7/dist-packages/wicd/dbusmanager.py", line 79, in
            connect_to_dbus
                proxy_obj = self._bus.get_object("org.wicd.daemon", '/org/wicd/daemon')
              File "/usr/lib/python2.7/dist-packages/dbus/bus.py", line 241, in
            get_object
                follow_name_owner_changes=follow_name_owner_changes)
              File "/usr/lib/python2.7/dist-packages/dbus/proxies.py", line 248, in
            __init__
                self._named_service = conn.activate_name_owner(bus_name)
              File "/usr/lib/python2.7/dist-packages/dbus/bus.py", line 180, in
            activate_name_owner
                self.start_service_by_name(bus_name)
              File "/usr/lib/python2.7/dist-packages/dbus/bus.py", line 278, in
            start_service_by_name
                'su', (bus_name, flags)))
              File "/usr/lib/python2.7/dist-packages/dbus/connection.py", line 651, in
            call_blocking
                message, timeout)
            dbus.exceptions.DBusException: org.freedesktop.DBus.Error.ServiceUnknown:
            The name org.wicd.daemon was not provided by any .service files

	:relevant post: http://ubuntuforums.org/showthread.php?t=1115640

#. Wicd's daemon
	
	So, Wicd's daemon isn't running. Lets check that::
	
		$  sudo service wicd status
		* wicd is not running
		 
	And try and start it?

	.. code:: pytb
	
            $  sudo service wicd start
            Traceback (most recent call last):
              File "/usr/share/wicd/daemon/wicd-daemon.py", line 1859, in <module>
                main(sys.argv)
              File "/usr/share/wicd/daemon/wicd-daemon.py", line 1708, in main
                os.symlink(dest, backup_location)
            OSError: [Errno 17] File exists

	Manually?

	.. code:: pytb

            $  sudo wicd -foe
            Traceback (most recent call last):
              File "/usr/share/wicd/daemon/wicd-daemon.py", line 1859, in <module>
                main(sys.argv)
              File "/usr/share/wicd/daemon/wicd-daemon.py", line 1708, in main
                os.symlink(dest, backup_location)
            OSError: [Errno 17] File exists

	Ahh, lets look for that issue.

#. Fixing daemon

	I found a bug on launchpad and basically, `/etc/resolve.conf` points
	towards a relative link rather than an absolute one so when wicd
	copies the symlink it points to a different path. I did this::

		# rm /etc/resolv.conf
		# ln -s /run/resolvconf/resolv.conf /etc/resolve.conf
		# rm /var/lib/wicd/resolv.conf.orig
		# ln -s /etc/resolv.conf /var/lib/wicd/resolv.conf.orig

	Then I retried starting up the daemon and success!

	:relevant bug: https://bugs.launchpad.net/ubuntu/+source/wicd/+bug/1132529
