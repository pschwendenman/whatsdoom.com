-------------------
Making time lapses
-------------------

:date: 2014-07-08
:category: blog
:author: Paul Schwendenman
:tags: mencoder, time lapse, photography, image magick, photography

Instructions
-------------

1. Make a new directory and copy only the images that are used for the timelapse.

  ::

	mkdir /tmp/tl
	cp timelapse*.jpg /tmp/tl/


#. Resize the images to a certain size. (Optional)::

	for i in *.JPG; do echo -e "$i...\c"; convert "$i" -resize 1920x1080 "$i"; echo done; done


#. Create a list of files::

	ls -1tr | grep -v files.txt > files.txt


  :note: This will use the order that is show by ``ls -1``, consider
	 changing the ls flags or adding ``sort`` to make a list sorted in
	 another way.


#. Create movie with a stream of jpgs::

	mencoder -nosound -noskip -oac copy -ovc copy -o output.avi -mf fps=15 'mf://@files.txt'


#. Compress the video into a 1080p video::

	ffmpeg -i output.avi -y -s hd1080 -sameq output-final.avi


Examples
---------

First example:

.. raw:: html

   <iframe width="640" height="360" src="//www.youtube.com/embed/GHmTkXzHzzs" frameborder="0" allowfullscreen></iframe>

Second example:

.. raw:: html

   <iframe width="640" height="360" src="//www.youtube.com/embed/dQPmKsM7pcM" frameborder="0" allowfullscreen></iframe>

Third Example:

.. raw:: html

   <iframe width="640" height="360" src="//www.youtube.com/embed/d34vNCAe3-w" frameborder="0" allowfullscreen></iframe>
