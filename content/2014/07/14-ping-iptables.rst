-------------------------
Allow ping with iptables
-------------------------

:date: 2014-07-14
:category: blog
:tags: ping, iptables, firewall, linux

Allow incoming::

  iptables -A INPUT  -p icmp --icmp-type 8 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
  iptables -A OUTPUT -p icmp --icmp-type 0 -m state --state ESTABLISHED,RELATED -j ACCEPT

Allow outgoing::

  iptables -A OUTPUT -p icmp --icmp-type 8 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
  iptables -A INPUT  -p icmp --icmp-type 0 -m state --state ESTABLISHED,RELATED -j ACCEPT

Deny::

  iptables -A OUTPUT -p icmp --icmp-type echo-request -j DROP


--------------


:original gist: https://gist.github.com/paul-schwendenman/179b94fe886cbd1503f8
