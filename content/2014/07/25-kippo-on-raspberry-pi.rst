---------------------------------
Setting up kippo on Raspberry Pi
---------------------------------

:date: 2014-07-25
:category: blog
:tags: kippo, python, raspberrypi, pypy


Downloading Kippo
------------------

1. Download tarball from Google code.

::

	wget https://code.google.com/p/kippo/downloads/detail?name=kippo-0.8.tar.gz

:note: the project has since moved to github_

.. _github: https://github.com/desaster/kippo

2. In my case most of the traffic was attempting a sftp connection. The
version of kippo that I acquired from Google code didn't handle sftp so I
found a fork on github.

::

	git clone https://github.com/micheloosterhof/kippo

This version ran extremely slow on the raspberry pi.

Diagnosing problems
---------------------

I ran::

  twistd -y kippo.tac -n --spew

And discovered that ``deepcopy`` was taking a long time.

Installing pypy
----------------

I wanted to see if pypy was able to run ``deepcopy`` faster.

.. code:: python

  r = []
  for i in xrange(40):
      l = [round(random.random()*100000) for i in xrange(10*3*60*60)]
      r.append(l)

  ini = time.time()
  copy.deepcopy(r)
  print "Time: ", time.time() - ini


:source: https://bugs.pypy.org/issue767

It was faster with pypy.

Installing third-party packages with pypy
==========================================

Roughly follow these instructions::

  $ curl -O http://python-distribute.org/distribute_setup.py
  $ curl -O https://raw.github.com/pypa/pip/master/contrib/get-pip.py
  $ ./pypy-2.1/bin/pypy distribute_setup.py
  $ ./pypy-2.1/bin/pypy get-pip.py
  $ ./pypy-2.1/bin/pip install pygments  # for example

:source: http://pypy.readthedocs.org/en/latest/getting-started.html

Install twisted and pycrypto

Running with pypy
------------------

::

  $ pypy /usr/bin/twisted -y kippo.tac -n

Change the start.py file to use pypy.


Starting kippo on boot
-----------------------

To make kippo start when the raspberry pi was booted I created an init file
the code can be found here:

https://gist.github.com/paul-schwendenman/94665208d9efbd0e2921#file-kippo
