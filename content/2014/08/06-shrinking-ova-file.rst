===================
Shrinking ova file
===================

:date: 2014-08-06
:tags: ova, virtual machines, vmware, resize ova
:category: blog


The Problem
------------

I wanted to export a virtual machine and send it to a customer. However when
I exported the OVA file, it was a massive 12GB.  I have exported this
virtual machine before and normally the ova is 2GB. 

First, I check to see if some log file has ballooned in size or some other
similar issue.  However ``du -h -d 0 /`` and ``df -h`` seemed to agree the used
disk space was only 6.2GB.

The Solution
-------------

Searching for causes of large ova files. I found a super user question that
suggested that vmdk files (contained in ova files) write the whole sector
for every sector that has a single non-zero bit.  Searching for ways to
compress vmdk files I found a page with this solution::

	cat /dev/zero >zero.fill; sleep 1; sync; sleep 1; /bin/rm -f zero.fill

This line will completely fill the disk with zeros, and then raise an error
once the disk is full.  Next, it sleeps for a second and then flushes the
file system buffers.  Lastly, after sleeping for another second, it removes
the zero filled file.

The Result
-----------

After running the command above on the guest operating system. I exported
the machine to a new ova.  This time the ova file was only 1.5 GB.  Much
better!

Sources
--------

- http://superuser.com/questions/502887/shrink-size-of-ova-file-in-virtualbox
- http://www.insanelymac.com/forum/topic/292301-vmware-workstation-how-to-compresscleanup-vmdk/
