---------------------------------
Colored diff on the Command line
---------------------------------

:date: 2014-10-31 12:04
:category: blog
:tags: cli tools, diff

If you wanted your ``diff`` output to be colored you have three options

1. Use a verison control program that outputs colored ``diff`` output

	::

		git diff --color=always old-file new-file | less -R

	:note: Mercurial does not allow you to run its ``hg diff`` outside of
		a repository

2. Use a text editor that allows for syntax highlighing of diff output.

	::

		diff old-file new-file | vim -R -

		diff old-file new-file | joe -rdonly - 

	:note: When using joe you may have to manually set the syntax
		highlighting with **^T Y diff [return]**

#. Use a wrapper for ``diff`` called ``colordiff``

	::

		colordiff old-file new-file
		diff old-file new-file | colordiff

	:note: that colordiff is just a wrapper and thus does not paginate
		adding a pipe to ``less -R`` would solve that issue
