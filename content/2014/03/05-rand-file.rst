---------------------------------------
Getting a random file from a directory
---------------------------------------

:date: 2014-03-05 12:35
:tags: bash, sh, random subset
:category: blog

I had a massive directory of files that I wanted to pick a few random files
to test before I imported the whole set.  For this post lets say I want 100
random files from some_dir and moving them to this_dir.

First you could use:

.. code::

	$ LIST=$(ls -1 ./some_dir | sort -R | tail -n 100)

but coreutils provides a shuffle program...

.. code::

	$ LIST=$(ls -1 ./some_dir | shuf -n 100)

From there you can copy the files using

.. code::

	$ for i in $LIST; do echo $i; cp "./some_dir/$i" ./this_dir/; done

Substituting in our variable:

.. code::

	$ for i in $(ls -1 ./some_dir | shuf -n 100); do echo $i; cp "./some_dir/$i" ./this_dir/; done

But that line looks messy because the source directory has to be repeated.

I think this one looks better, but to use this we can to use find to output
the relative path:

for i in $(find ./some_files/ -print -type f | shuf -n 10); do echo $i; cp "$i" ./this_dir/; done

Next we can throw that in a function:

.. code::

	cp_rand_subset ()
	{
		for i in $(find $2 -print -type f | shuf -n $1); do echo $i; cp "$i" $3; done
	}

And after defining it call it like:

.. code::

	cp_rand_subset 100 some_dir this_dir

Or

.. code::

	cp_rand_subset 57 ./lots_files /home/share/samples/
