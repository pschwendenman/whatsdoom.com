------------------------------------
Setting feh as default image viewer
------------------------------------

:date: 2014-06-09
:category: blog
:tags: ubuntu, feh, image viewer, gnome settings

I found eog to be too slow at rendering pictures for my purpose. I wanted to
make feh (a commandline based image viewer) my default.

1. Try and figure out how to change default.

   - right click image
   - properties
   - open with

   but I couldn't find feh listed as an option

#. Edit feh's .desktop file

   - open /usr/share/applications/feh.desktop
   - change a bunch of stuff

   still cannot get feh to show up.

#. Change the mime type default manually

   - edit ".local/share/applications/mimeapps.list" to map all image opens to feh.

     .. code:: diff

        8,23c8,23
        < image/bmp=gpicview.desktop
        < image/gif=gpicview.desktop
        < image/jpeg=gpicview.desktop
        < image/jpg=gpicview.desktop
        < image/png=gpicview.desktop
        < image/tiff=gpicview.desktop
        < image/x-bmp=gpicview.desktop
        < image/x-pcx=gpicview.desktop
        < image/x-tga=gpicview.desktop
        < image/x-portable-pixmap=gpicview.desktop
        < image/x-portable-bitmap=gpicview.desktop
        < image/x-targa=gpicview.desktop
        < image/x-portable-greymap=gpicview.desktop
        < application/pcx=gpicview.desktop
        < image/svg+xml=gpicview.desktop
        < image/svg-xml=gpicview.desktop
        ---
        > image/bmp=feh.desktop
        > image/gif=feh.desktop
        > image/jpeg=feh.desktop
        > image/jpg=feh.desktop
        > image/png=feh.desktop
        > image/tiff=feh.desktop
        > image/x-bmp=feh.desktop
        > image/x-pcx=feh.desktop
        > image/x-tga=feh.desktop
        > image/x-portable-pixmap=feh.desktop
        > image/x-portable-bitmap=feh.desktop
        > image/x-targa=feh.desktop
        > image/x-portable-greymap=feh.desktop
        > application/pcx=feh.desktop
        > image/svg+xml=feh.desktop
        > image/svg-xml=feh.desktop

#. Change the default feh command by copying /usr/share/applications/feh.desktop to ~/.local/share/applications

   .. code:: diff

      11c11
      < Exec=feh
      ---
      > Exec=feh -Z %U

