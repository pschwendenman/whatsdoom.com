================================
Installing node to a virtualenv
================================

:date: 2014-06-18
:category: blog
:tags: virtualenv, virtual environment, node, python

I had originally typed up these instructions in a gist for my personal use
but in case they are useful to you:

1. Change directory to virtualenv::

        cd /path/to/virtualenv

#.  Download node::

        wget http://nodejs.org/dist/v0.10.29/node-v0.10.29.tar.gz

#. Set prefix variable::

        export VPREFIX=$(pwd)

#. Untar the archive::

        tar xfz node-v0.10.29.tar.gz

#. Change to that new directory::

        cd node-v0.10.29.tar.gz

#. Run configure::

        ./configure --prefix=$VPREFIX

   Note: you should see something about you virtualenv's python in the configure output

#. Run make::

        make

#. Install node::

        make install

#. Verify the correct path::

        which node

   :note: you have to have you virtualenv activated.


:original gist: https://gist.github.com/paul-schwendenman/6303074
