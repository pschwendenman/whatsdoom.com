-----------------------
View git diff by word
-----------------------

:date: 2017-01-08
:category: blog
:tags: git

By default, ``git diff`` will highlight differences by line.

Differences highlighted word by word::

    $ git diff --word-diff

Differences highlighted character by character::

    $ git diff --word-diff-regex=.

Differences highlighted word by word without extra characters::

    $ git diff --color-words

Differences highlighted character by character without extra characters::

    $ git diff --color-words=.

More information can be found on these options in the manual.
