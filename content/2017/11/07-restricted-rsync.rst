----------------------------------
Restricting rsync access with SSH
----------------------------------

:date: 2017-11-07
:category: blog
:tags: ssh, rsync

SSH public keys can be added to ``~/.ssh/authorized_keys`` on a
typical system to allow the holder of the private key to access
the system. Sometimes however you might want to restrict the access
a particular key has.

In my case, I wanted my CI system to be able to push my static site
after the build was complete. I didn't want the CI system to have
unrestricted access to my server via private key. After a little bit
of research I found ``rrsync``.

Using authorized_keys to restrict key
--------------------------------------

SSH provides an option to restrict the client to using a single
command in the AuthorizedKeysFile.

:command="command": Specifies that the
    command is executed whenever this key
    is used for authentication. The command
    supplied by the user (if any) is ignored.


SSH provides many other options which can be used to further limit the SSH
connection.

:no-agent-forwarding:
    forbids authentication agent forwarding
:no-port-forwarding:
    forbids TCP forwarding
:no-pty:
    prevents tty allocation
:no-user-rc:
    disables execution of ``~/.ssh/rc``.
:no-X11-forwarding:
    forbids X11 forwarding
:restrict:
    Enable all restrictions, i.e. disable port, agent and
    X11forwarding, as well as disabling PTY allocation
    and execution of ~/.ssh/rc.

Introducing ``rrsync``
------------------------

The ``rsync`` package has a helper script that facilitates restricting ``rsync``

It can be extracted in Ubuntu from ``/usr/share/``::

     gunzip --to-stdout /usr/share/doc/rsync/scripts/rrsync.gz > ~/bin/rrsync

The restricted rsync command takes a single argument, the sub directory to
restrict the user's actions.

::

    command="$HOME/bin/rrsync /path/to/subdir/"

The ``-ro`` flag can be used to allow only read-only rsync commands.

::

    command="$HOME/bin/rrsync -ro /path/to/subdir/"

Adding key to authorized_keys file
-----------------------------------

Putting it all together::

    command="$HOME/bin/rrsync /path/to/subdir/",no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding ssh-rsa {PUBLIC_KEY}

Adding the line above to would prevent the client from accessing any commands
other than rsync and also sandbox the user inside ``/path/to/subdir``

Or simply::

    command="$HOME/bin/rrsync /path/to/subdir/",restrict ssh-rsa {PUBLIC_KEY}


Sources
--------

Documentation:

- `SSH documentation`_
- `command option docs`_

Related Q&A:

- `Allow the restricted rsync (rrsync) script for arbitrary directories with authorized_keys`_
- `how to add commands in authorized_keys`_

Related Blogs:

- `Securing rsync with SSH keys`_
- `Restricting SSH Access to rsync`_

.. _SSH Documentation: http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man8/sshd.8
.. _command option docs: http://man.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man8/sshd.8#command="command"
.. _Allow the restricted rsync (rrsync) script for arbitrary directories with authorized_keys:
    https://unix.stackexchange.com/questions/276198/allow-the-restricted-rsync-rrsync-script-for-arbitrary-directories-with-author
.. _how to add commands in authorized_keys:
    https://stackoverflow.com/questions/26171836/how-to-add-commands-in-ssh-authorized-keys
.. _Securing rsync with SSH keys:
    https://moepman.eu/securing-rsync-with-ssh-keys.html
.. _Restricting SSH Access to rsync:
    https://www.guyrutenberg.com/2014/01/14/restricting-ssh-access-to-rsync/
