=================================================
Setting Default Values in Bash Variables
=================================================

:status: draft
:date: 2016-09-19
:category: blog
:tags: bash

.. table:: Reference Table
    :class: table table-striped

    ================== ========
    ${parameter:=word} parameter set
    ================== ========


http://unix.stackexchange.com/questions/122845/using-a-b-for-variable-assignment-in-scripts
http://wiki.bash-hackers.org/syntax/pe
