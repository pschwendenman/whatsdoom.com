Consul notes
-------------

:status: draft
:date: 2020-02-25


- https://www.consul.io/docs/
- https://learn.hashicorp.com/consul
- https://learn.hashicorp.com/consul/new-release/overview
- https://www.digitalocean.com/community/tutorials/how-to-secure-consul-with-tls-encryption-on-ubuntu-14-04




- https://learn.hashicorp.com/vault
- https://learn.hashicorp.com/nomad

- https://nomadproject.io/guides/integrations/consul-integration/
- https://nomadproject.io/guides/install/
- https://nomadproject.io/docs/configuration/consul/
