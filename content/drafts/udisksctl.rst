Ubuntu 16.04 udiskctl
-------------------------

:status: draft
:date: 2016-10-01

::

    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl --help
    Unknown command `--help'
    Usage:
      udisksctl COMMAND

    Commands:
      help            Shows this information
      info            Shows information about an object
      dump            Shows information about all objects
      status          Shows high-level status
      monitor         Monitor changes to objects
      mount           Mount a filesystem
      unmount         Unmount a filesystem
      unlock          Unlock an encrypted device
      lock            Lock an encrypted device
      loop-setup      Set-up a loop device
      loop-delete     Delete a loop device
      power-off       Safely power off a drive
      smart-simulate  Set SMART data for a drive

    Use "udisksctl COMMAND --help" to get help on each command.

    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl unmount /media/paul/Main/
    Usage:
      udisksctl unmount [OPTION...]

    Unmount a filesystem.

    Options:
      -p, --object-path         Object to unmount
      -b, --block-device        Block device to unmount
      -f, --force               Force/lazy unmount
      --no-user-interaction     Do not authenticate the user if needed

    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl unmount -p /media/paul/Main/

    (udisksctl unmount:5438): GLib-GIO-CRITICAL **: g_dbus_object_manager_get_object: assertion 'g_variant_is_object_path (object_path)' failed
    Error looking up object with path /media/paul/Main/
    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl unmount -b /dev/sdc1
    Unmounted /dev/sdc1.
    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl unmount -b /dev/sdc2
    Unmounted /dev/sdc2.
    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl status -b /dev/sdc
    Usage:
      udisksctl status

    Shows high-level status.

    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl status
    MODEL                     REVISION  SERIAL               DEVICE
    --------------------------------------------------------------------------
    SSD2SC240G1CS1754D117-820 CS111101  PNY07150000716031208 sda
    WDC WD6400BEVT-75A0RT0    01.01A01  WD-WXV1E40LYH08      sdb
    ST750LM022 HN-M750MBB     2AR10001  S2XGJ9AC814794       sdc
    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl info
    Usage:
      udisksctl info [OPTION...]

    Show information about an object.

    Options:
      -p, --object-path      Object to get information about
      -b, --block-device     Block device to get information about
      -d, --drive            Drive to get information about

    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl info -b /dev/sdc
    /org/freedesktop/UDisks2/block_devices/sdc:
      org.freedesktop.UDisks2.Block:
        Configuration:              []
        CryptoBackingDevice:        '/'
        Device:                     /dev/sdc
        DeviceNumber:               2080
        Drive:                      '/org/freedesktop/UDisks2/drives/ST750LM022_HN_M750MBB_S2XGJ9AC814794'
        HintAuto:                   true
        HintIconName:
        HintIgnore:                 false
        HintName:
        HintPartitionable:          true
        HintSymbolicIconName:
        HintSystem:                 false
        Id:                         by-id-ata-ST750LM022_HN-M750MBB_S2XGJ9AC814794
        IdLabel:
        IdType:
        IdUUID:
        IdUsage:
        IdVersion:
        MDRaid:                     '/'
        MDRaidMember:               '/'
        PreferredDevice:            /dev/sdc
        ReadOnly:                   false
        Size:                       750156373504
        Symlinks:                   /dev/disk/by-id/ata-ST750LM022_HN-M750MBB_S2XGJ9AC814794
                                    /dev/disk/by-id/wwn-0x50004cf20845c164
                                    /dev/disk/by-path/pci-0000:00:1d.0-usb-0:1.2:1.0-scsi-0:0:0:0
      org.freedesktop.UDisks2.PartitionTable:
        Type:               dos
    paul@paul-Inspiron-N5010 ~/p/m/scripts> udisksctl power-off -b /dev/sdc
    paul@paul-Inspiron-N5010 ~/p/m/scripts>
