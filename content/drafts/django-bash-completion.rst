=================================================
Enable bash completion for Django in virtualenv
=================================================

:status: draft
:date: 2016-09-05
:category: blog
:tags:

::

    echo 'source "${VIRTUAL_ENV}/src/django/extras/django_bash_completion"' >> ${VIRTUAL_ENV}/bin/postactivate

https://coderwall.com/p/5y1oqq/automatically-enable-django-s-bash-completion-on-virtualenv-activate-using-virtualenvwrapper
