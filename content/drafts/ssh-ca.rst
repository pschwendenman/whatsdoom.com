SSH CA Notes
-------------

:status: draft
:date: 2020-02-25

Notes:

- https://smallstep.com/blog/use-ssh-certificates/
- https://github.com/smallstep/step-ssh-example/blob/master/Vagrantfile


::

    $ step certificate inspect https://whatsdoom.com


Install the CA cert for validating user certificates
(from ``~/.ssh/certs/ssh_user_key.pub`` on the CA).

::

    $ echo "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPrvMrLPN/zxEfKS5phzZWeAzc6Lv21xxZWkkz7EhDH0VeygkigsU8RYGLKMjTlHXUxc8CQxI8OzrSVUvRJ5//I=" > $(step path)/certs/ssh_user_ca_key.pub
