--------------------------------
Replacing and Old Key with New
--------------------------------

:status: draft
:date: 2016-10-30
:category: blog
:tags: gpg, pgp
:series: gpg and keybase

Download private key from keybase
----------------------------------

::

    $ keybase pgp export -q 35D161033AD9847F -s | gpg --import
    gpg: key 3AD9847F: secret key imported
    gpg: key 3AD9847F: "Paul Schwendenman <schwendenman.paul@gmail.com>" not changed
    gpg: Total number processed: 1
    gpg:              unchanged: 1
    gpg:       secret keys read: 1
    gpg:   secret keys imported: 1

Find Old Key and New key
-------------------------

::

    gpg --list-secret-keys
    /home/paul/.gnupg/secring.gpg
    -----------------------------
    sec   2048R/2A8E8375 2014-03-28 [expires: 2019-03-27]
    uid                  Paul R Schwendenman (Shared) <schwendenman.paul@gmail.com>
    ssb   2048R/D1369210 2014-03-28

    sec   4096R/3AD9847F 2016-10-27 [expires: 2032-10-23]
    uid                  Paul Schwendenman <schwendenman.paul@gmail.com>
    ssb   4096R/E17563C6 2016-10-27


Trusting a new key
--------------------

In the interactive session, trust and sign the new key

open interactive session::

    gpg --edit-key 3AD9847F
    gpg (GnuPG) 1.4.20; Copyright (C) 2015 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    pub  4096R/3AD9847F  created: 2016-10-27  expires: 2032-10-23  usage: SC
                         trust: unknown       validity: unknown
    sub  4096R/E17563C6  created: 2016-10-27  expires: 2032-10-23  usage: E
    [ unknown] (1). Paul Schwendenman <schwendenman.paul@gmail.com>

    gpg>

trust new key::

    gpg> trust
    pub  4096R/3AD9847F  created: 2016-10-27  expires: 2032-10-23  usage: SC
                         trust: unknown       validity: unknown
    sub  4096R/E17563C6  created: 2016-10-27  expires: 2032-10-23  usage: E
    [ unknown] (1). Paul Schwendenman <schwendenman.paul@gmail.com>

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

      1 = I don't know or won't say
      2 = I do NOT trust
      3 = I trust marginally
      4 = I trust fully
      5 = I trust ultimately
      m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    pub  4096R/3AD9847F  created: 2016-10-27  expires: 2032-10-23  usage: SC
                         trust: ultimate      validity: unknown
    sub  4096R/E17563C6  created: 2016-10-27  expires: 2032-10-23  usage: E
    [ unknown] (1). Paul Schwendenman <schwendenman.paul@gmail.com>
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.


sign new key with old::

    gpg> sign 2A8E8375

    pub  4096R/3AD9847F  created: 2016-10-27  expires: 2032-10-23  usage: SC
                         trust: ultimate      validity: unknown
     Primary key fingerprint: B350 336C A863 CE2C ACD3  3BD8 35D1 6103 3AD9 847F

         Paul Schwendenman <schwendenman.paul@gmail.com>

    This key is due to expire on 2032-10-23.
    Are you sure that you want to sign this key with your
    key "Paul R Schwendenman <schwendenman.paul@gmail.com>" (3AD9847F)

    Really sign? (y/N) y

    You need a passphrase to unlock the secret key for
    user: "Paul R Schwendenman <schwendenman.paul@gmail.com>"
    2048-bit RSA key, ID 3AD9847F, created 2014-01-27



save and exit::

    gpg> save
    $


push keys to keyserver, i.e. mit::

    gpg --keyserver pgp.mit.edu --send-keys 2A8E8375 3AD9847F
    gpg: sending key 2A8E8375 to hkp server pgp.mit.edu
    gpg: sending key 3AD9847F to hkp server pgp.mit.edu



other
------

::

    gpg --list-keys --list-options show-uid-validity

Source

https://www.apache.org/dev/key-transition.html

https://lists.gnupg.org/pipermail/gnupg-users/2004-January/021237.html
http://askubuntu.com/questions/135050/gpg-command-list-sigs-only-from-trusted-keys
http://irtfweb.ifa.hawaii.edu/~lockhart/gpg/gpg-cs.html
https://futureboy.us/pgp.html
