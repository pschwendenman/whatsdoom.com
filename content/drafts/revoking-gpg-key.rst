------------------
Revoking GPG key
------------------

:status: draft
:date: 2016-11-12

::

    paul@paul-Inspiron-N5010 ~> gpg --list-keys
    /home/paul/.gnupg/pubring.gpg
    -----------------------------
    pub   2048R/2A8E8375 2014-03-28 [expires: 2021-11-12]
    uid                  Paul R Schwendenman (Shared) <schwendenman.paul@gmail.com>
    sub   2048R/D1369210 2014-03-28 [expires: 2021-11-12]

::

    paul@paul-Inspiron-N5010 ~> gpg --output revoke-2A8E8375.asc --gen-revoke 2A8E8375

    sec  2048R/2A8E8375 2014-03-28 Paul R Schwendenman (Shared) <schwendenman.paul@gmail.com>

    Create a revocation certificate for this key? (y/N) y
    Please select the reason for the revocation:
      0 = No reason specified
      1 = Key has been compromised
      2 = Key is superseded
      3 = Key is no longer used
      Q = Cancel
    (Probably you want to select 1 here)
    Your decision? 1
    Enter an optional description; end it with an empty line:
    >
    Reason for revocation: Key has been compromised
    (No description given)
    Is this okay? (y/N) y

    You need a passphrase to unlock the secret key for
    user: "Paul R Schwendenman (Shared) <schwendenman.paul@gmail.com>"
    2048-bit RSA key, ID 2A8E8375, created 2014-03-28

    ASCII armored output forced.
    Revocation certificate created.

    Please move it to a medium which you can hide away; if Mallory gets
    access to this certificate he can use it to make your key unusable.
    It is smart to print this certificate and store it away, just in case
    your media become unreadable.  But have some caution:  The print system of
    your machine might store the data and make it available to others!


https://www.gnupg.org/gph/en/manual/c14.html
https://www.hackdiary.com/2004/01/18/revoking-a-gpg-key/
