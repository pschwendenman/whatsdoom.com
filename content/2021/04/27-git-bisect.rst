========================
Playing with git bisect
========================


:status: draft

::

    git bisect start
    git bisect bad
    git checkout master
    git bisect good


::

    git bisect start
    git bisect bad HEAD
    git bisect good 17a9b2f

::

    git bisect run ./check.sh

::

    #!/bin/bash

    cd web/
    npm run build

::

    git bisect start HEAD 17a9b2f

::

    #!/bin/bash

    git apply fix.patch

    cd web/
    npm run build

    status=$?

    git reset --hard

    exit $status


Sources:

- https://git-scm.com/docs/git-bisect#_examples
- https://stackoverflow.com/questions/29930048/git-bisect-with-additional-patch
