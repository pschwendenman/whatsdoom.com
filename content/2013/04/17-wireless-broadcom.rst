=================================
Broadcom Wireless Internet Setup
=================================

:date: 2013-04-17 2:20
:tags: linux, wireless, broadcom
:category: blog

So I have had this issue where my wireless card would one day suddenly
disappear.  I had a wireless usb so I used that for a month or so and
finally I decided that I would just reinstall and see if that fixed it.  It
did like magic but then a couple days later poof gone.  Now if you are
reading this there is a good chance you have search for linux and broadcom
related information and you have found a ton.  Ultimately I fixed mine using
information from a few sources:

First you can list your network related hardware using::

	$ sudo lshw -C network

Confirm that you have a broadcom chip. In my case the broadcom chip was
listed as "``*``-network UNCLAIMED".  Next you can check for some sort of
blocking

::

	$ rfkill list all

In my case before I fixed the issue my wireless device wasn't even listed.
After that I would run::

	$ lspci -vnn | grep 14e4:

In my case after finding that command I check back on the table at
http://linuxwireless.org/en/users/Drivers/b43#Supported_devices.  My device
was listed as 14e4:4727 which is BCM4313 (same as lshw listed) Next I looked
at http://linuxwireless.org/en/users/Drivers/brcm80211 I found my chip in a
table listed under " brcmsmac" from my research I knew that "brcmsmac" was
one of the options for broadcom drivers.  So I tried modprobing it as I had
done for 'b43' and 'brcmfmac' earlier.

::

	$ sudo modprobe brcmsmac

And success!

Lastly if you don't want to run that command every time you turn your
machine on you could add "modprobe brmsmac" on a line immediately before
"exit 0" in the file /etc/rc.local


The four most helpful and complete sources:
https://wiki.archlinux.org/index.php/Broadcom_wireless#broadcom-wl
http://linuxwireless.org/en/users/Drivers/brcm80211
http://linuxwireless.org/en/users/Drivers/b43#Supported_devices
http://askubuntu.com/questions/235279/my-wifi-adapter-is-not-working-at-all-where-to-start-troubleshooting




