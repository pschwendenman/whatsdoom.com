====================
Extension ram files
====================

:date: 2013-04-22 2:40
:tags: .ram files, ffmpeg, linux, mplayer, ubuntu
:category: blog

I was helping my girlfriend watch some videos that she needed for a class
that she was having trouble playing on her mac.  Once I saw that ubuntu
wasn't able to play them out of the box I looked at the extension and found
a solution on ubuntu forums.

The solution suggested that I try mplayer to play the file::

	$ mplayer -playlist file.ram

Success! I could watch the video streaming.

However, it also pointed out that extension ram files were just real
player's playlist and the file was simply a text file.  I opened the file
and it contained one line which was the url to the video.  A .rm file, real
players video file, that I was sure I could covert with ffmpeg.

The url was rather long so I used cat and wget to download the whole file.

::

	$ wget `cat file.ram`


Next, I used a tutorial on ehow to find the correct flags for converting
using ffmpeg.  (I am lazy and why read man pages when you have examples you
can search and find)

::

	$ ffmpeg -i IN.rm -vcodec mpeg4 -sameq -acodec aac -ab OUT.mp4

It seems I am never going to remember that avconv is the new replacement for
ffmpeg.  But no matter what I tried I couldn't seem to get it to work.

I decided because I knew it would run in mplayer that mencoder was the logical choice.

::

	$ mencoder IN.rm -aid 0 -vid 1 -ovc lavc -oac lavc -o OUT.mp4


And that .... worked a little but the video seemed poorly made.

Next i tried something called transmageddon which was a gui and seemed to
spin rather than work.  More to come if I get it to work...


Sources:

http://ubuntuforums.org/showthread.php?t=1177723

http://www.ehow.com/how_6306550_convert-realmedia-mp4-using-linux.html
