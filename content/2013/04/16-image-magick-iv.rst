========================================
Watermarking with Image Magick
========================================

:date: 2013-04-16 3:41
:tags: photography, image magick, watermark
:category: blog
:series: Image Magick

I decided to try and start signing my pictures.

::

	composite -watermark 15% -gravity southeast sign-1.png $i $i;

It needs more tweaking but that will do for now.

To convert all files in a directory:

.. code:: sh

	for i in *.JPG; do
		echo -e "$i... \c";
		composite -watermark 15% -gravity southeast sign-1.png $i $i;
		echo done;
	done


Oneliner:

.. code:: sh

	$ for i in *.JPG; do echo -e "$i... \c"; composite -watermark 15% -gravity southeast sign-1.png $i $i; echo done; done
