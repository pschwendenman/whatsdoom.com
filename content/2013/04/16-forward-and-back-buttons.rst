===========================
Forward and Back Buttons 
===========================

:date: 2013-04-16 8:18 
:tags: javascript
:category: blog

For what ever reason perhaps just nostalgia possibly because I happen to use
them more than expected, I have forward and back links on my website.  The
buttons are JavaScript powered and I have had "noscript" tags warning users
of their lack of functionality since I added the links years ago.  However,
I don't like that they still exist without scripting.  Anyway, I decided
that I was going to use scripting to draw the buttons.

.. code:: javascript

    <script type="text/javascript"> document.write('\x3Ca
                href="javascript:history.back()">Back\x3C/a>');
                document.write('\x3Ca
                href="javascript:history.forward()">Forward\x3C/a>');
                document.write('\x3Chr />');
    </script>

Note: that by using '\x3C' rather than '<', I eliminated a potential problem
in the parsing of the page and made it so that they would validate again.

http://stackoverflow.com/questions/236073/why-split-the-script-tag-when-writing-it-with-document-write

