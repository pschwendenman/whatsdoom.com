=========
pygments
=========

:date: 2013-02-22 14:52
:tags: pygments
:category: blog

Pygments is a python based syntax highlighter.

Examples::

	pygmentize -f html -l python -O full -o file.py.html  file.py

	pygmentize -l latex -O full -o resume.tex.html resume.tex
