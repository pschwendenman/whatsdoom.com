-----------
LVM resize
-----------

:date: 2013-07-25 23:59
:tags: lvm, bash, sh
:category: blog

My root partition recently ran out of space and I needed to resize it.

So I booted into a live usb and then, after activating my volume groups:

.. code::

	$ sudo vgchange -a y

I looked up lvm resize online and came up with the command:

.. code::

	$ sudo lvextend -L+10G /dev/mapper/big-root

Next I rebooted and ran "df -h". It was the same size and I was annoyed and
confused.  After some routine poking around I noticed the man page suggested
the use of a "-r" flag to actually resize the file system.  Who knew?

Final command:

.. code::

	$ sudo lvextend -r -L+10G /dev/mapper/big-root


PS: Oh and if you need to reduce the size of the partition because you ran
the command without modifying the filesystem and you don't want to learn how
to use "resizefs"

.. code::

	$ sudo lvreduce -L-10G /dev/mapper/big-root


UPDATE: A reader has suggested that the previous code block might be
misleading.  To reduce the size of a lvm partition, you should use the
following command with the "-r" flag or you will risk losing data.  The file
system must be resized before the lvm partition's size is reduced.  However
in the previous example, my filesystem was the size of the partion after
reduction.

.. code::

	$ sudo lvreduce -r -L-10G /dev/mapper/big-root


Note: Commandlinefu says it better and faster:
http://www.commandlinefu.com/commands/view/12195/lvextend-logical-volume
