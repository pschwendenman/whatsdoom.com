==========
Play DVDs
==========

:date: 2013-03-12 10:14
:tags: linux, dvd, debian, ubuntu
:category: blog

To play dvds first you must install a package::

	sudo apt-get install libdvdread4

Next you will run this command and like magic it works::

	sudo /usr/share/doc/libdvdread4/install-css.sh

You may need to reboot:

http://askubuntu.com/questions/500/how-can-i-play-encrypted-dvd-movies
