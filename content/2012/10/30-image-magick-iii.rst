======================================
Inverting an Image with Image Magick
======================================

:date: 2012-10-30
:tags: photography, invert image, image magick
:category: blog
:series: Image Magick

Three ways to invert an image::

  convert black-white.png -negate white-black.png
  convert upside-down.png -rotate 180 rightside-up.png
  convert upside-down.png -flip rightside-up.png

:source: http://www.imagemagick.org/pipermail/magick-users/2003-October/011071.html
