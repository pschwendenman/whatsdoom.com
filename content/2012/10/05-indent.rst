=======
indent
=======

:date: 2012-10-05 12:43
:tags: cli tools, indent
:category: blog

Indent is an program that can reformat C code for you.

Today, someone emailed me their strangely formatted code and I decided
before I even looked at it that I would run it through "indent." After
looking through the man page for the "correct" flags, I settled on a style.

I personally like my code to look roughly like this::

	$  indent -bad -bap -bl -blf -bls -i4 -nbfda -nprs -npsl -saf -sai -saw filename
