=============
MLA in latex
=============

:date: 2012-05-22 1:41
:tags: MLA, latex
:category: blog

I wish that I had know about latex back in high school when I was writing my MLA styled papers.
For example the following template could be used to make a MLA paper for the user really simply.

.. code:: latex

    \documentclass[12pt,letterpaper]{article}
    \usepackage{ifpdf}
    \usepackage{mla}
    \begin{document}
    \begin{mla}{Your First Name}{Your Last Name}{Professor's last name}{Class name}{Date}{Title}

    Body

    \begin{workscited}

    \bibent
    author's last name, first name.  ``Paper Title."  \textit{Book Title}.  Date
    of publication.

    \end{workscited}
    \end{mla}
    \end{document}
