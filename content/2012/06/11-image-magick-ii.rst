====================================
Image Resizing with Image Magick II
====================================

:date: 2012-06-11 3:09
:tags: photography, image resize, image magick
:category: blog
:series: Image Magick

I already posted about image magick before but since this time I actually needed to use it, I found the "for" statement to work better than find.

::

	$ for i in *.jpg; do echo "$i"; convert "$i" -resize 600x "$i"; done

Edited::

	$ for i in *.JPG; do echo -e "$i...\c"; convert "$i" -resize 600x "$i"; echo done; done
