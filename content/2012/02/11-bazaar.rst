=======
Bazaar
=======

:date: 2012-02-11 16:22
:tags: bazaar, dvcs
:category: blog

A presentation I gave to the Open Source club as an introduction to Bazaar:

Introduction
-------------

Bazaar is a distrubuted revision control system sponsored by Canonical

- written in python
- packages for GNU/Linux, Mac OS X, and Windows.
- FOSS
- intuitive commands
- easy to use
- support for working with other systems
	- git
	- cvs
- mercurial
- source code hosting
	- launchpad
	- sourceforge
	- gnu savannah




Install and Setup Bazaar
-------------------------

Install::

	sudo apt-get install bzr

Setup::

	bzr whoami "Paul <paul@opensource.osu.edu>"

Starting a project
--------------------

New
####


Recommended::

	bzr init-repo sample
	cd sample
	bzr init trunk
	cd trunk

What I do::

	mkdir sample
	bzr init

Existing
##########

New branch::

	bzr branch mine new

New checkout::

	bzr checkout src mine

New 'lightweight' checkout use the --lightweight flag

View Repo Status
------------------

To view the status (a list of any changed or added files as well as any shelved items)::

	bzr status

To view the log::

	bzr log

Which will look something like::

        $ bzr log
        ------------------------------------------------------------
        revno: 2
        committer: John Doe <john.doe@gmail.com>
        branch nick: myproject
        timestamp: Mon 2007-10-08 17:56:14 +0000
        message:
          Added first line of text
        ------------------------------------------------------------
        revno: 1
        committer: John Doe <john.doe@gmail.com>
        branch nick: myproject
        timestamp: Mon 2006-10-08 17:46:22 +0000
        message:
          Initial import

To view the differences since last commit::

	bzr diff

Optionally you can use::

	bzr diff [filename]

to only see the changes of one file.
The diff output can be directed to a standard editor.
It looks like this::

    $ bzr diff
    === modified file 'test1.txt'
    --- test1.txt   2007-10-08 17:56:14 +0000
    +++ test1.txt   2007-10-08 17:46:22 +0000
    @@ -0,0 +1,1 @@
    +test test test

Also by default diff compares the file to the most recent commit.
To compare to an older version you can use the -r flag which also works for commands like branch::

	bzr diff -r 1..3 [filename]

File Manipulation
------------------

Adding and Removing
####################

To start "versioning" a file or directory use::

	bzr add [path]

To remove or "un-version" a file or directory use::

	bzr remove --keep [filename]

That would save the file (not delete it) as opposed to::

	bzr remove --force [filename]

Ignoring
##########

To ignore a file use::

	bzr ignore [filename]

To list ignored files use::

	bzr ignored

Moving, Cat...
###############

To move a file you can use::

	bzr mv [old] [new]

This is important because otherwise you would lose the history of a file if you just moved it by normal means.
To see the contents of a file in a previous revision you can "cat" the file.

::
	bzr cat -r32 [filename]

Version Control
################

Committing::

	bzr commit

	bzr commit -m "Message"

	bzr commit [filename] -m "Message"

:note: if you use "bzr commit" it will open your default editor ($EDITOR)

Uncommitting::

	bzr uncommit

If you want to restore the files to the last commit you can revert them::

	bzr revert

Another handy tool is shelving. If you make changes to code but don't have time to finish it or decide to go in a different direction you can shelve the changes::

	bzr shelve

Merging
########

To merge two branches::

	bzr merge other
	bzr commit

Sometimes you may have conflicts that will need fixed::

	bzr resolve [filename]

To update a checkout you can simply run::

	bzr update

Publishing
------------

Lauchpad
#########

I personally have never used this.

Pushing via ssh
################

To push code from my laptop to my server I often use the following command::

	bzr push bzr+ssh://sarcasm.ath.cx/home/paul/tictac

The same can be done to pull changes from, to lets say, stallman (our club server)::

	bzr pull bzr+ssh://sarcasm.ath.cx/home/paul/tictac

Another feature of bzr is to 'export' the current revision::

	bzr export ../distri

Or as an archive::

	bzr export ../distri.tar.gz
	bzr export ../distri.tar.bz2
	bzr export ../distri.zip

There is also an import to reverse the process::

	bzr import ../distri.tar.bz2

Help
-----

The most important part of bzr is the help.
Running the command::

	bzr help [command]

will tell you additional information about the command.
To find a list of commands you can use::

	bzr help commands

Lastly just running "bzr help" yeilds::

  Bazaar 2.1.4 -- a free distributed version-control tool
  http://bazaar-vcs.org/

  Basic commands:
    bzr init           makes this directory a versioned branch
    bzr branch         make a copy of another branch

    bzr add            make files or directories versioned
    bzr ignore         ignore a file or pattern
    bzr mv             move or rename a versioned file

    bzr status         summarize changes in working copy
    bzr diff           show detailed diffs

    bzr merge          pull in changes from another branch
    bzr commit         save some or all changes
    bzr send           send changes via email

    bzr log            show history of changes
    bzr check          validate storage

    bzr help init      more help on e.g. init command
    bzr help commands  list all commands
    bzr help topics    list all help topics

