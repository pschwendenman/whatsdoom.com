============
Grub issues
============

:date: 2012-02-20 9:41
:tags: grub
:category: blog

I killed my grub a enough times that I wrote this "script" to chroot into my system from a live cd / usb

To chroot in to a system with lvm2::


  yes | sudo apt-get install lvm2
  sudo vgchange -ay big
  sudo fdisk -l /sda
  sudo fdisk -l /dev/sda
  sudo mkdir /tmp/mnt
  sudo mkdir /tmp/mnt/boot

  sudo mount /dev/big/boot /tmp/mnt/boot
  sudo mount /dev/big/root/ /tmp/mnt

  sudo mount --bind /dev /tmp/mnt/dev
  sudo mount --bind /proc /tmp/mnt/proc
  sudo mount --bind /boot /tmp/mnt/boot

  sudo chroot /tmp/mnt

Some things that might fix Grub::

  nano -w /etc/default/grub

  grub-install /dev/sda
  grub-install --recheck /dev/sda

  yes | sudo apt-get install --reinstall libdebian-installer4
  sudo os-prober
  sudo update-grub


Exit and unmount::

  exit
  sudo umount /tmp/mnt/dev
  sudo umount /tmp/mnt/proc
  sudo umount /tmp/mnt/
  sudo umount /tmp/mnt2 
