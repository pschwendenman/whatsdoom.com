Image Resizing with Image Magick
=================================

:date: 2011-12-22 2:35
:tags: photography, resize image, image magick
:category: blog
:series: Image Magick

To batch resize images using image magick:

.. code:: bash

	find . -name “*.jpg” -exec convert {} -resize 800×600 {} \;

:source: http://ubuntuchocolate.wordpress.com/2007/09/22/howto-batchmass-image-resize/
