Dual Displays
===============

:date: 2011-12-23 9:58
:tags: dual display, linux, x11
:category: blog

I was using awesome window manager and trying to connect an additional
monitor to my computer with a vga port.  A friend recommended ``xrandr``.  A
quick look at the man page and you'll find::

	xrandr -q

will list off all the connected display devices. I see four listed: LVDS1,
HDMI1, VGA1, DP1.  The two that were connected were LVDS1 and VGA1.  So then
after some playing with setting the resolution I found '--auto' so now a
quick::

	xrandr --output VGA1 --auto --right-of LVDS1 --auto

Works like a charm. If you prefer gui applications look into ``arandr``.
