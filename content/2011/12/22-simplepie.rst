SimplePie
===========

:date: 2011-12-22 5:18
:tags: project spotlight, RSS
:category: blog
:summary: SimplePie project spotlight.


I have embedded my RSS feed from my blogs into my website using Simple Pie. It was really easy to do.

http://simplepie.org/

If you download the zip from their site you can simply copy the example from the documentation:

http://simplepie.org/wiki/setup/sample_page

And have this working in now time at all. For a really helpful screen cast:

http://css-tricks.com/video-screencasts/55-adding-rss-content-with-simplepie/

The project is hosted on github:

https://github.com/simplepie/simplepie
