-----------
Brightness
-----------

:date: 2011-12-30 3:46
:tags: linux, brightness
:category: blog
:slug: brightness

If you want to adjust your screen brightness in Linux (Debian / Ubuntu) from the command line you can write to a file::

	/sys/devices/virtual/backlight/acpi_video0/brightness

The maximum value for this variable is listed in the file::

	/sys/devices/virtual/backlight/acpi_video0/max_brightness

A script can easily be written to have a variable argument:

.. code:: sh

  if [ $# -eq 0 ]; then
          VAR="0"
  else
          VAR=`echo $1`
  fi
  sudo sh -c "echo $VAR > /sys/devices/virtual/backlight/acpi_video0/brightness"

