#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Paul Schwendenman'
SITENAME = 'whatsdoom.com'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'
I18N_TEMPLATES_LANG = 'en'

# THEME = 'mytheme'
THEME="whatsdoom-theme"
# CUSTOM_CSS = 'static/custom.css'
BANNER = 'images/banner.jpg'

PYGMENTS_STYLE = "native"

#Plugins
PLUGINS = [
    "tag_cloud",
    "sitemap",
    "series",
    "readtime",
    "neighbors",
]

JINJA_ENVIRONMENT = {}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

#Url stuff
ARTICLE_URL = 'posts/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/index.html'
DAY_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%m}/{date:%d}/index.html'

DRAFT_URL = 'drafts/{slug}.html'
DRAFT_SAVE_AS = 'drafts/{slug}.html'

TAG_URL = 'tags/{slug}/'
TAG_SAVE_AS = 'tags/{slug}/index.html'
TAGS_URL = 'tags/'
TAGS_SAVE_AS = 'tags/index.html'

AUTHOR_URL = 'authors/{slug}/'
AUTHOR_SAVE_AS = 'authors/{slug}/index.html'
AUTHORS_URL = 'authors/'
AUTHORS_SAVE_AS = 'authors/index.html'

CATEGORY_URL = 'categories/{slug}/'
CATEGORY_SAVE_AS = 'categories/{slug}/index.html'
CATEGORIES_URL = 'categories/'
CATEGORIES_SAVE_AS = 'categories/index.html'

ARCHIVES_URL = 'archives/'
ARCHIVES_SAVE_AS = 'archives/index.html'

SERIES_URL = 'series/'
SERIES_SAVE_AS = 'series/index.html'

# Blogroll
LINKS = (
    ('websites', '/links/'),
    ('affiliates', '/affiliates/'),
)

# Social widget
SOCIAL = (
    ('twitter', 'https://twitter.com/whatsdoom', 'twitter'),
    ('gitlab', 'https://gitlab.com/pschwendenman', 'gitlab'),
    ('github', 'https://github.com/paul-schwendenman', 'github'),
    ('bitbucket', 'https://bitbucket.org/pschwendenman', 'bitbucket'),
    ('flickr', 'http://www.flickr.com/photos/schwendenman-paul/', 'flickr'),
    ('reddit', 'https://www.reddit.com/user/whatsdoom', 'reddit'),
    ('instagram', 'https://www.instagram.com/whatsdoom/', 'instagram'),
    # ('google+', 'https://plus.google.com/102610109265716621807/about', 'google-plus'),
    ('linkedin', 'https://www.linkedin.com/in/paul-schwendenman-317aa09a', 'linkedin'),
    ('facebook', 'https://www.facebook.com/paul.schwendenman', 'facebook'),
    ('keybase', 'https://keybase.io/whatsdoom', 'keybase')
)

DEFAULT_PAGINATION = 10
DEFAULT_ORPHANS = 3
PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    # (1, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
    (2, '{base_name}/{number}/', '{base_name}/{number}/index.html'),
)

STATIC_PATHS = [
    'extra/robots.txt',
    'extra/favicon.ico',
    'extra/resume.pdf',
    'extra/40D9D08813E47FC4.txt',
    'static/custom.css',
    'images/banner.jpg',
    'images/profile.jpg',
]

EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/favicon.ico': {'path': 'favicon.ico'},
    'extra/resume.pdf': {'path': 'resume.pdf'},
    'extra/40D9D08813E47FC4.txt': {'path': '40D9D08813E47FC4.txt'},
    'static/custom.css': {'path': 'static/custom.css'},
    'images/banner.jpg': {'path': 'images/banner.jpg'},
    'images/profile.jpg': {'path': 'images/profile.jpg'},
}

# DIRECT_TEMPLATES = (('index', 'tags', 'categories', 'archives', 'search'))
DIRECT_TEMPLATES = (('index', 'tags', 'categories', 'archives', 'authors', 'series'))

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
USE_PAGER = False

# Article info options
SHOW_SERIES=True
SHOW_ARTICLE_AUTHOR=False
SHOW_ARTICLE_CATEGORY=False
SHOW_DATE_MODIFIED=True

# Tag cloud settings
TAG_CLOUD = True
TAG_CLOUD_STEPS = 4
TAG_CLOUD_MAX_ITEMS = 30
TAG_CLOUD_BADGE = False

# Pelican Bootstrap3 Theme settings
DISPLAY_TAGS_INLINE = True
DISPLAY_ARTICLE_INFO_ON_INDEX = True

DISPLAY_BREADCRUMBS = True
DISPLAY_CATEGORY_IN_BREADCRUMBS = True

DISPLAY_RECENT_POSTS_ON_SIDEBAR = True
RECENT_POST_COUNT = 5

ABOUT_ME = 'software developer. electrical engineer. linux enthusiast. amateur photographer'
AVATAR = 'images/profile.jpg'

FAVICON = 'favicon.ico'

# GITHUB_USER="paul-schwendenman"
# GITHUB_REPO_COUNT=3
# GITHUB_SKIP_FORK=False
# GITHUB_SHOW_USER_LINK=False

# SHARIFF = True
# SHARIFF_LANG="en"
# SHARIFF_ORIENTATION="vertical"
# SHARIFF_THEME="gray"
# SHARIFF_TWITTER_VIA=True

CC_LICENSE = "CC-BY-NC-SA"

# Sitemap
SITEMAP = {
    'format': 'xml',
    'exclude': [
        'tag/',
        'category/',
        'author/',
    ],
    'priorities': {
        'articles': 0.8,
        'indexes': 0.5,
        'pages': 0.9
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'weekly',
        'pages': 'monthly'
    }
}
